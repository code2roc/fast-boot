<p align="center">
	<img alt="logo" src="https://code2roc-image.oss-cn-beijing.aliyuncs.com/fastboot.png" width="150" height="150">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">FastBoot</h1>
<h4 align="center">一个基于企业级项目框架演化而来的快速开发内核，让开发变得简单、快捷！</h4>
<hr style="height:1px;border:none;border-top:1px solid #ccc;" />

## 前言：
- [在线文档：https://gitee.com/code2roc/fast-boot/tree/master/mdbook/](https://gitee.com/code2roc/fast-boot/tree/master/mdbook/)

- 欢迎大家在各种服务器环境下测试，一起完善项目，有问题请提issues

- 开源不易，点个 star 鼓励一下吧！


## FastBoot 介绍

**FastBoot** 是基于企业级项目框架演化而来的快速开发内核，提高开发效率专注于业务逻辑

提供数据库交互操作模板，集成ehcache/redis/quartz等第三方组件

内置常用过滤器拦截器，系统安全防护等功能，提供基于layui开发的后台管理页面

你只需要进行以下步骤就可以快速使用

- 安装环境，mysql5.7，redis3.2.1，jdk1.8
- 导入code文件夹下项目
- 创建数据库（编码utf8mb4，排序规则utf8mb4_general_ci）
- 执行脚本（fastboot.sql）,修改数据库连接
- 直接运行访问 http://localhost:9001/fastboot/index.html
- 用户名：sysadmin  密码：1