var nowdate = new Date();
var version = ConvertDateToString(nowdate);
LoadIconAndTitle();

function GetRootPath() {
    var loc = window.location,
        host = loc.hostname,
        protocol = loc.protocol,
        port = loc.port ? (':' + loc.port) : '';
    var path = location.pathname;

    if (path.indexOf('/') === 0) {
        path = path.substring(1);
    }

    var mypath = '/' + path.split('/')[0];
    path = (mypath != undefined ? mypath : ('/' + loc.pathname.split('/')[1])) + '/';

    var rootPath = protocol + '//' + host + port + path;
    return rootPath;
}

function LoadCSS(url, isappendversion) {
    var configCookie = $.cookie(cookieConfig);
    if (configCookie && configCookie!=null && configCookie!="undefined") {
        var config = JSON.parse($.cookie(cookieConfig));
        var configVersion = config.staticFileVersion;
        if (configVersion) {
            version = configVersion;
        }
    }
    if (isappendversion) {
        url += "?version=" + version;
    }
    document.write('<link rel="stylesheet" href= ' + url + ' media="all"></link>');
}

function LoadJS(url, isappendversion) {
    var configCookie = $.cookie(cookieConfig);
    if (configCookie && configCookie!=null && configCookie!="undefined") {
        var config = JSON.parse($.cookie(cookieConfig));
        var configVersion = config.staticFileVersion;
        if (configVersion) {
            version = configVersion;
        }
    }
    if (isappendversion) {
        url += "?version=" + version;
    }
    document.write('<script src= ' + url + '></script>');
}

function LoadIconAndTitle() {
    var configCookie = $.cookie(cookieConfig);
    if (configCookie && configCookie!=null && configCookie!="undefined") {
        var config = JSON.parse($.cookie(cookieConfig));
        document.title = config.systemName
    }
    var iconurl = GetRootPath() + "favicon.png" + "?version=" + version;
    document.write('<link rel="shortcut icon" href= ' + iconurl + '  rel="external nofollow" rel="external nofollow" ></link>');
}



LoadJS(GetRootPath() + 'js/common.js', true);
LoadJS(GetRootPath() + 'js/http.js', true);
LoadJS(GetRootPath() + 'plugins/layuiadmin/layui/layui.js', false);
LoadCSS(GetRootPath() + 'plugins/layuiadmin/layui/css/layui.css', false);
LoadJS(GetRootPath() + 'plugins/layuiextend/dialog.js', true);
LoadJS(GetRootPath() + 'plugins/layuiextend/table.js', true);
LoadJS(GetRootPath() + 'plugins/layuiextend/form.js', true);
LoadJS(GetRootPath() + "plugins/jqueryvalidate/jquery.validate.min.js", false)
LoadJS(GetRootPath() + "plugins/jqueryvalidate/jquery.validate.message_cn.js", false)
LoadCSS(GetRootPath() + 'css/form.css', true);
LoadCSS(GetRootPath() + 'css/boot.css', true);
LoadCSS(GetRootPath() + 'css/list.css', true);
LoadJS(GetRootPath() + 'plugins/ztree/js/jquery.ztree.all.min.js', false);
LoadCSS(GetRootPath() + 'plugins/ztree/css/metro/metroStyle.css', false);
LoadJS(GetRootPath() + 'js/ztree.js', true);
LoadJS(GetRootPath() + 'plugins/layuiextend/treeSelectSingle.js', true);
LoadJS(GetRootPath() + 'plugins/layuiextend/richText.js', true);
LoadJS(GetRootPath() + 'plugins/sparkmd5/spark-md5.min.js', false);
LoadCSS(GetRootPath() + 'plugins/upload/upload.css', true);
LoadJS(GetRootPath() + 'plugins/upload/upload.js', true);
