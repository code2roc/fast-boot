function BindTable(id, url, params, callback, openpage) {
    var pager = true;
    if (typeof openpage == "undefined") {
        pager = true;
    } else {
        pager = openpage;
    }
    var postparams = {};
    if (params) {
        postparams = params;
    }
    layui.use('table', function() {
        var table = layui.table;
        table.reload(id, {
            method: 'post',
            contentType: "application/json;charset=uft-8",
            url: GetApiPath() + url + "?token=" + GetTokenID(),
            page: pager ? { curr: 1 } : false,
            where: postparams,
            request: {
                pageName: 'pageIndex', //页码的参数名称，默认：page
                limitName: 'pageSize' //每页数据量的参数名，默认：limit
            },
            height: 'full',
            parseData: function(res) { //res 即为原始返回的数据
                if (pager) {
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.data.total, //解析数据长度
                        "data": res.data.rows //解析数据列表
                    };
                } else {
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.data.obj.length, //解析数据长度
                        "data": res.data.obj //解析数据列表
                    };
                }

            },
            done: function(res, curr, count) {
                BindTableTool(id);
                if (callback) {
                    callback(res, curr, count);
                }
            }

        });
    });
}

function BindTableV2(id, cols, url, params, callback, openpage) {
    var pager = true;
    if (typeof openpage == "undefined") {
        pager = true;
    } else {
        pager = openpage;
    }
    var postparams = {};
    if (params) {
        postparams = params;
    }
    layui.use('table', function() {
        var table = layui.table;
        table.reload(id, {
            cols: cols,
            method: 'post',
            url: GetApiPath() + url + "?token=" + GetTokenID(),
            contentType: "application/json;charset=uft-8",
            page: pager ? { curr: 1 } : false,
            where: postparams,
            request: {
                pageName: 'pageIndex', //页码的参数名称，默认：page
                limitName: 'pageSize' //每页数据量的参数名，默认：limit
            },
            parseData: function(res) { //res 即为原始返回的数据
                if (pager) {
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.data.total, //解析数据长度
                        "data": res.data.rows //解析数据列表
                    };
                } else {
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.data.obj.length, //解析数据长度
                        "data": res.data.obj //解析数据列表
                    };
                }
            },
            done: function(res, curr, count) {
                BindTableTool(id);
                if (callback) {
                    callback(res, curr, count);
                }
            }

        });
    });
}

function RefreshTable(id, TableData) {
    layui.table.init(id, {
        data: TableData
    });
}

function BindTableCellEdit(id, callback) {
    layui.table.on('edit(' + id + ')', function(obj) {
        var field = obj.field //得到字段
        var value = obj.value //得到修改后的值
        var data = obj.data; //得到所在行所有键值
        var update = {};
        update[field] = value;
        obj.update(update);
        if (callback) {
            callback(layui.table.getData(id))
        }
    });
}

function BindTableTool(id) {
    layui.use('table', function() {
        var table = layui.table;
        table.on('tool(' + id + ')', function(obj) {
            var data = obj.data
            var layEvent = obj.event;
            eval(layEvent)(data)
        });

    });
}

function GetEnumCodeText(codeName, codeValue) {
    var page = window;
    var text = "";
    if (!page[codeName]) {
        BindEnumCode(codeName, function(result, status) {
            page[codeName] = result.data.obj;
        })
    }
    page[codeName].forEach(element => {
        if (element.itemValue == codeValue) {
            text = element.itemText
        }
    });
    return text;
}

function GetCheckData(id) {
    var tableid = "datagrid";
    if (id && id != "") {
        tableid = id;
    }
    var checkStatus = layui.table.checkStatus(tableid);
    var data = checkStatus.data;
    return data;
}

function GetTableData(id) {
    var tableid = "datagrid";
    if (id && id != "") {
        tableid = id;
    }
    var tableData = layui.table.getData(tableid);
    $(".layui-table-main .table-input").each(function(index, item) {
        var id = $(item).attr("id");
        var value = $(item).val();
        var field = id.split("|")[0];
        var row_id = id.split("|")[1];
        tableData.forEach(element => {
            if (element.row_id == row_id) {
                element[field] = value;
            }
        });
    })
    $(".layui-table-main .table-select").each(function(index, item) {
        var id = $(item).attr("id");
        var value = $(item).val();
        var field = id.split("|")[0];
        var row_id = id.split("|")[1];
        tableData.forEach(element => {
            if (element.row_id == row_id) {
                element[field] = value;
            }
        });
    })
    return tableData;
}

function BindTableSelect(id, codeName, defaultValue) {
    var page = window;

    if (!page[codeName]) {
        BindEnumCode(codeName, function(result, status) {
            page[codeName] = result.data.obj;
        })
    }
    var html = "";
    html += " <select id='" + id + "' lay-ignore class='layui-border table-select'>"
    page[codeName].forEach(element => {
        if (element.itemValue == defaultValue) {
            html += " <option  value='" + element.itemValue + "' selected>" + element.itemText + "</option>"
        } else {
            html += " <option  value='" + element.itemValue + "' >" + element.itemText + "</option>"
        }

    });
    html += "</select>"
    return html;
}