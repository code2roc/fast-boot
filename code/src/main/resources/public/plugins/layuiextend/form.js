function UseForm() {
    layui.use('form', function() {})
    layui.use('laydate', function() {
        var laydate = layui.laydate;
        laydate.render({
            elem: '.layui-date',
            type: 'date'
        });
        laydate.render({
            elem: '.layui-time',
            type: 'time'
        });
    });
    layui.use('tinymce', function() {
        var tinymce = layui.tinymce;
        tinymce.render({
            elem: '.layui-richtext',
        });
    })
}

function SubmitForm(formid, url, beforesubmit, params, callback) {
    var formvalidator = $("#" + formid).validate();
    if (formvalidator.form()) {
        var formData = GetForm(formid)
        //合并表单数据和传递的额外参数，若key相同，以表单数据为准
        var postdata = {};
        if (params) {
            for (var attr in params) {
                postdata[attr] = params[attr];
            }
        }
        for (var attr in formData) {
            postdata[attr] = formData[attr];
        }
        if (beforesubmit) {
            if (beforesubmit(postdata)) {
                HttpPost(JSON.stringify(postdata), url, function(result, status) {
                    callback(result, status)
                })
            }
        } else {
            HttpPost(JSON.stringify(postdata), url, function(result, status) {
                callback(result, status)
            })
        }
    }
    return false;
}


function SetForm(formid, url, params, callback) {
    layui.use('form', function() {
        HttpPost(JSON.stringify(params), url, function(result, status) {
            if (result.code == 0) {
                BindForm(formid, result.data.obj)
                if (callback) {
                    callback(result.data.obj);
                }
            } else {
                OpenFail(result.msg)
            }
        })
    });
}

function BindForm(formid, data) {
    var form = layui.form;
    form.val(formid, data);
    var obj = $("#" + formid);
    $.each(data, function(name, value) {
        //switch
        var $switchList = obj.find("input[name='" + name + "'][type='checkbox'][lay-skin='switch']");
        if ($switchList.length > 0) {
            var $switch = $($switchList[0]);
            var switchvalSetting = $switch.attr("lay-value").split("|");
            var settingJSON = {};
            if (switchvalSetting[0] == value.toString()) {
                //open
                settingJSON[name] = true;
            } else {
                //close
                settingJSON[name] = false;
            }
            form.val(formid, settingJSON);
        }
        //checkbox
        var $checkBoxList = obj.find("input[name='" + name + "'][type='checkbox'][lay-skin='primary']");
        if ($checkBoxList.length > 0) {
            var settingValueList = value.split("|");
            for (var i = 0; i < $checkBoxList.length; i++) {
                if (settingValueList.indexOf($checkBoxList[i].value) >= 0) {
                    $checkBoxList[i].checked = true;
                } else {
                    $checkBoxList[i].checked = false;
                }
            }
        }
        //single-tree
        var $singleTreeList = obj.find("input[name='" + name + "'][class~='layui-single-tree']");
        if($singleTreeList .length>0){
            layui.use(["treeSelect"], function() {
                BindSelectTreeNode(name,value);
            })
          
        }

         //richText
         layui.use('tinymce', function() {
            var tinymce = layui.tinymce;
            var $richTextList = obj.find("textarea[name='" + name + "'][class='layui-richtext']");
            if ($richTextList.length > 0) {
                var edit =tinymce.get("#" + name)
                edit.setContent(value);
            }
        })
        form.render();
    })
}

function GetForm(formid) {
   var form = layui.form; 
    var formData = form.val(formid);
    var obj = $("#" + formid);

    //switch
    var $switchList = obj.find("input[type='checkbox'][lay-skin='switch']");
    if ($switchList.length > 0) {
        $($switchList).each(function() {
            var $switch = $(this);
            var name = $switch.attr("name");
            var switchvalSetting = $switch.attr("lay-value").split("|");
            if (formData[name] == undefined) {
                formData[name] = switchvalSetting[1]
            } else {
                formData[name] = switchvalSetting[0]
            }
        })
    }

    $.each(formData, function(name, value) {
        //checkbox
        var $checkBoxList = obj.find("input[name='" + name + "'][type='checkbox'][lay-skin='primary']");
        if ($checkBoxList.length > 0) {
            var result = [];
            $checkBoxList.each(function() {
                if ($(this).is(":checked")) {
                    result.push($(this).attr("value"));
                }
            });
            var checkBoxData = result.join("|");
            formData[name] = checkBoxData
        }
        var $richTextList = obj.find("textarea[name='" + name + "'][class='layui-richtext']");
        if ($richTextList.length > 0) {
            var edit = layui.tinymce.get("#" + name)
            formData[name] = edit.getContent();
        }

    })
    return formData;
}

function DisableForm(formid){
    $("input").attr("disabled","disabled");
    $("select").attr("disabled","disabled");
    $("textarea").attr("disabled","disabled");
     //richText
     layui.use('tinymce', function() {
        var tinymce = layui.tinymce;
        var $richTextList = $("textarea[class='layui-richtext']");
        if ($richTextList.length > 0) {
            $($richTextList).each(function(){
                var id = $(this).attr("id");
                var edit =tinymce.get("#" + id)
                edit.destroy();
            })
        }
    })
    layui.use('tinymce', function() {
        var tinymce = layui.tinymce;
        tinymce.render({
            elem: '.layui-richtext',
            readonly: true,
            plugins : false,
            toolbar:false,
            statusbar:false,
            menubar: false,
        });
    })
    layui.form.render();
    DisableAttachControl();
     //single-tree
     var $singleTreeList = $("input[class~='layui-single-tree']");
     if($singleTreeList .length>0){
         layui.use(["treeSelect"], function() {
            DisableSelectTree();
         })
       
     }
}

function BindRadio(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindEnumCode(codeName, function(result, status) {
        if (result.code == 0) {
            result.data.obj.forEach(element => {
                if (element.itemValue == defaultValue) {
                    html += " <input type='radio' name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "' checked='true'  lay-filter='" + name + "' " + disabled + ">"
                } else {
                    html += " <input type='radio' name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "'  lay-filter='" + name + "' " + disabled + ">"
                }

            });
            ele.replaceWith(html);
            var form = layui.form;
            form.render($("input[name='" + name + "']"));
            if (change) {
                form.on("radio(" + name + ")", function(data) {
                    change(data.value);
                });
            }
        }
    })
}

function BindSettingRadio(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindSettingCode(codeName, function(result, status) {
        if (result.code == 0) {
            result.data.obj.forEach(element => {
                if (element.itemValue == defaultValue.toString()) {
                    html += " <input type='radio' name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "' checked='true'  lay-filter='" + name + "' " + disabled + ">"
                } else {
                    html += " <input type='radio' name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "'  lay-filter='" + name + "' " + disabled + ">"
                }

            });
            ele.replaceWith(html);
            var form = layui.form;
            form.render($("input[name='" + name + "']"));
            if (change) {
                form.on("radio(" + name + ")", function(data) {
                    change(data.value);
                });
            }
        }
    })
}

function BindSelect(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindEnumCode(codeName, function(result, status) {
        if (result.code == 0) {
            html += " <select name='" + name + "' lay-filter='" + name + "' " + disabled + ">"
            result.data.obj.forEach(element => {
                if (element.itemValue == defaultValue) {
                    html += " <option  value='" + element.itemValue + "' selected>" + element.itemText + "</option>"
                } else {
                    html += " <option  value='" + element.itemValue + "' >" + element.itemText + "</option>"
                }

            });
            html += "</select>"
            ele.replaceWith(html);
            var form = layui.form;
            form.render($("select[name='" + name + "']"));
            if (change) {
                form.on("select(" + name + ")", function(data) {
                    change(data.value);
                });
            }

        }
    })
}

function BindSettingSelect(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindSettingCode(codeName, function(result, status) {
        if (result.code == 0) {
            html += " <select name='" + name + "' lay-filter='" + name + "' " + disabled + ">"
            result.data.obj.forEach(element => {
                if (element.itemValue == defaultValue.toString()) {
                    html += " <option  value='" + element.itemValue + "' selected>" + element.itemText + "</option>"
                } else {
                    html += " <option  value='" + element.itemValue + "' >" + element.itemText + "</option>"
                }

            });
            html += "</select>"
            ele.replaceWith(html);
            if (change) {
                form.on("select(" + name + ")", function(data) {
                    change(data.value);
                });
            }

        }
    })
}

function BindCheckBox(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindEnumCode(codeName, function(result, status) {
        if (result.code == 0) {
            var defaultValueList = defaultValue.split("|");
            result.data.obj.forEach(element => {
                if (defaultValueList.indexOf(element.itemValue.toString()) >= 0) {
                    html += " <input lay-skin='primary' type='checkbox'  name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "' checked='true'  lay-filter='" + name + "' " + disabled + ">"
                } else {
                    html += " <input lay-skin='primary' type='checkbox' name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "'  lay-filter='" + name + "' " + disabled + ">"
                }

            });
            ele.replaceWith(html);
            if (change) {
                form.on("checkbox(" + name + ")", function(data) {
                    change(data.value);
                });
            }
        }
    })
}

function BindSettingCheckBox(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindSettingCode(codeName, function(result, status) {
        if (result.code == 0) {
            var defaultValueList = defaultValue.split("|");
            result.data.obj.forEach(element => {
                if (defaultValueList.indexOf(element.itemValue.toString()) >= 0) {
                    html += " <input lay-skin='primary' type='checkbox'  name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "' checked='true'  lay-filter='" + name + "' " + disabled + ">"
                } else {
                    html += " <input lay-skin='primary' type='checkbox' name='" + name + "' value='" + element.itemValue + "' title='" + element.itemText + "'  lay-filter='" + name + "' " + disabled + ">"
                }

            });
            ele.replaceWith(html);
            if (change) {
                form.on("checkbox(" + name + ")", function(data) {
                    change(data.value);
                });
            }
        }
    })
}

function BindSwitch(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindEnumCode(codeName, function(result, status) {
        if (result.code == 0) {
            var switchtext = "";
            var switchvalue = "";
            if (result.data.obj.length = 2) {
                var on = result.data.obj[0];
                var off = result.data.obj[1];
                switchtext = on.itemText + "|" + off.itemText;
                switchvalue = on.itemValue + "|" + off.itemValue;
                if (defaultValue == on.itemValue) {
                    html += " <input lay-skin='switch' type='checkbox'  checked  name='" + name + "' " + disabled + " lay-text='" + switchtext + "' lay-value='" + switchvalue + "'>"
                } else {
                    html += " <input lay-skin='switch' type='checkbox'  name='" + name + "' " + disabled + " lay-text='" + switchtext + "' lay-value='" + switchvalue + "'>"
                }

            }
            ele.replaceWith(html);
            if (change) {
                form.on("switch(" + name + ")", function(data) {
                    change(data.value);
                });
            }
        }
    })
}

function BindSettingSwitch(name, codeName, defaultValue, change) {
    var ele = $("input[name='" + name + "']");
    var disabled = $(ele).attr("disabled") === undefined ? '' : 'disabled';
    var html = "";
    BindSettingCode(codeName, function(result, status) {
        if (result.code == 0) {
            var switchtext = "";
            var switchvalue = "";
            if (result.data.obj.length = 2) {
                var on = result.data.obj[0];
                var off = result.data.obj[1];
                switchtext = on.itemText + "|" + off.itemText;
                switchvalue = on.itemValue + "|" + off.itemValue;
                if (on.itemValue == defaultValue.toString()) {
                    html += " <input lay-skin='switch' type='checkbox'  checked  name='" + name + "' " + disabled + " lay-text='" + switchtext + "' value='" + switchvalue + "'>"
                } else {
                    html += " <input lay-skin='switch' type='checkbox'  name='" + name + "' " + disabled + " lay-text='" + switchtext + "' value='" + switchvalue + "'>"
                }

            }
            ele.replaceWith(html);
            if (change) {
                form.on("switch(" + name + ")", function(data) {
                    change(data.value);
                });
            }
        }
    })
}