layui.extend({
    treeSelect: '{/}' + GetRootPath() + "plugins/treeselect/treeSelect"
  })

function BindSelectTree(id, url, param, defaultvalue, callback, defaulttext) {
    var text = "请选择"
    if (defaulttext) {
        text = defaulttext;
    }
    layui.use(["treeSelect"], function() {
        var treeSelect = layui.treeSelect;
        HttpPost_Sync(JSON.stringify(param), url, function(result, status) {
            var nodes = result.data.obj;
            treeSelect.render({
                // 选择器
                elem: '#' + id,
                // 数据
                data: nodes,
                placeholder: text,
                // 是否开启搜索功能：true/false，默认false
                search: true,
                // 点击回调
                click: function(d) {

                },
                // 加载完成后的回调函数
                success: function(d) {
                    if(defaultvalue && defaultvalue!=null && defaultvalue!=""){
                        treeSelect.checkNode(id, defaultvalue);
                    }
                    if (callback) {
                        callback(treeSelect, d)
                    }
                },
                style: {
                    folder: { // 父节点图标
                        enable: true // 是否开启：true/false
                    },
                    line: { // 连接线
                        enable: true // 是否开启：true/false
                    }
                },
            });
        })

    })
}

function BindSelectTreeNode(id,value){
    var treeSelect = layui.treeSelect;
    treeSelect.checkNode(id, value);
}

function DisableSelectTree(){
    $(".layui-select-title").unbind()
}