function FormAjax_Sync(token, data, url, success) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("post", url, false);
    xmlHttp.setRequestHeader("token", token);
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.status == 200) {
            var result = JSON.parse(this.responseText);
            var status = this.status
            success(result, status);
        }
    };
    xmlHttp.send(data);

}

onmessage = function(evt) {
    var data = evt.data;
    console.log(data)
    //传递的参数
    var token = data.token
    var attachGuid = data.attachGuid
    var uploadUrl = data.uploadUrl
    var filename = data.filename
    var fileMD5 = data.fileMD5
    var groupguid = data.groupguid
    var grouptype = data.grouptype
    var chunkCount = data.chunkCount
    var chunkSize = data.chunkSize
    var filesize = data.filesize
    var filename = data.filename
    var file = data.file



    var start = 0;
    var end;
    var index = 0;
    var startTime = new Date().getTime();
    while (start < filesize) {
        end = start + chunkSize;
        if (end > filesize) {
            end = filesize;
        }
        var chunk = file.slice(start, end); //切割文件    
        var formData = new FormData();
        formData.append("file", chunk, filename);
        formData.append("attachGuid", attachGuid);
        formData.append("fileMD5", fileMD5);
        formData.append("chunkCount", chunkCount)
        formData.append("chunkIndex", index);
        formData.append("chunkSize", end - start);
        formData.append("group_guid", groupguid);
        formData.append("group_type", grouptype);
        //上传文件
        FormAjax_Sync(token, formData, uploadUrl, function(result, status) {
            var code = 0;
            var percent = 0;
            if (result.code == 0) {
                console.log("分片共" + chunkCount + "个" + ",已成功上传第" + index + "个")
                percent = parseInt((parseInt(formData.get("chunkIndex")) + 1) * 100 / chunkCount);
            } else {
                filesize = -1;
                code = -1
                console.log("分片第" + index + "个上传失败")
            }
            self.postMessage({ code: code, percent: percent });
        })
        start = end;
        index++;
    }
    console.log("上传分片总时间：" + (new Date().getTime() - startTime));
    console.log("分片完成");
}
