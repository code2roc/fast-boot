package ${packageName}.dao;

import ${packageName}.model.${classModelName};;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ${className} extends BaseMapper<${classModelName}>{

}