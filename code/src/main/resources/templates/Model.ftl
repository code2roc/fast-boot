package ${packageName}.model;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;
import java.util.Date;
import java.math.BigDecimal;

@TableName("${tableName}")
public class ${className} extends BaseBootModel{

    @TableId(value = "row_id")
    private String row_id;

    @TableField(value = "sort_num")
    private Integer sort_num;

    @TableField(value = "gmt_create")
    private Date gmt_create;

    @TableField(value = "gmt_modified")
    private Date gmt_modified;

<#if columnList ? exists>
    <#list columnList as model>
        @TableField(value = "${model.columnName}")
        <#if model.columnType = 'text'>
            private String ${model.columnName};
        </#if>
        <#if model.columnType = 'int'>
            private Integer ${model.columnName};
        </#if>
        <#if (model.columnType = 'numeric')>
            private BigDecimal ${model.columnName};
        </#if>
        <#if model.columnType = 'date'>
            private Date ${model.columnName};
        </#if>

    </#list>
</#if>

    @Override
    public String getRow_id() {
    return row_id;
    }

    @Override
    public void setRow_id(String row_id) {
    this.row_id = row_id;
    }

    @Override
    public Integer getSort_num() {
    return sort_num;
    }

    @Override
    public void setSort_num(Integer sort_num) {
    this.sort_num = sort_num;
    }

    @Override
    public Date getGmt_create() {
    return gmt_create;
    }

    @Override
    public void setGmt_create(Date gmt_create) {
    this.gmt_create = gmt_create;
    }

    @Override
    public Date getGmt_modified() {
    return gmt_modified;
    }

    @Override
    public void setGmt_modified(Date gmt_modified) {
    this.gmt_modified = gmt_modified;
    }

<#if columnList?exists>

    <#list columnList as model>
        <#if model.columnType = 'text'>
            public String get${model.columnName?cap_first}() {
                return ${model.columnName};
            }

            public void set${model.columnName?cap_first}(String ${model.columnName}) {
                this.${model.columnName} = ${model.columnName};
            }
        </#if>
        <#if model.columnType = 'int'>
            public Integer get${model.columnName?cap_first}() {
                return ${model.columnName};
            }

            public void set${model.columnName?cap_first}(Integer ${model.columnName}) {
                this.${model.columnName} = ${model.columnName};
            }
        </#if>
        <#if model.columnType = 'numeric'>
            public BigDecimal get${model.columnName?cap_first}() {
                return ${model.columnName};
            }

            public void set${model.columnName?cap_first}(BigDecimal ${model.columnName}) {
                this.${model.columnName} = ${model.columnName};
            }
        </#if>
        <#if model.columnType = 'date'>
            public Date get${model.columnName?cap_first}() {
                return ${model.columnName};
            }

            public void set${model.columnName?cap_first}(Date ${model.columnName}) {
                this.${model.columnName} = ${model.columnName};
            }
        </#if>

    </#list>
</#if>
}