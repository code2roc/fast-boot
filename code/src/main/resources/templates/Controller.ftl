package ${packageName}.controller;

import ${packageName}.model.${classModelName};
import ${packageName}.service.I${className}Service;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Page;
import com.code2roc.fastboot.framework.submitlock.SubmitLock;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("${rounterName}")
public class ${className}Controller extends BaseBootController {

	@Autowired
	private I${className}Service ${className?uncap_first}Service;

	@ResponseBody
	@PostMapping("/list")
	public Object list(@RequestBody Map<String, Object> params){
		Result result = Result.okResult();
		HashMap<String, Object> paramMap = new HashMap<>();
		String sql = "1=1";
		Page page = ${className?uncap_first}Service.selectPage("*", sql, "sort_num desc",paramMap);
		result.add("rows", page.getRows());
		result.add("total", page.getTotal());
		return result;
	}

	@ResponseBody
	@PostMapping("/insert")
	@SubmitLock
	@Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
	public Object insert(@RequestBody ${classModelName} entity) {
		Result result =Result.okResult();
		${className?uncap_first}Service.insert(entity);
		return result;
	}

	@ResponseBody
	@PostMapping("/detail")
	public Object detail(@RequestBody Map<String, Object> params) {
		Result result = Result.okResult();
		String row_id =ConvertOp.convert2String(params.get("row_id"));
		result.add("obj", ${className?uncap_first}Service.selectOne(row_id));
		return result;
	}

	@ResponseBody
	@PostMapping("/update")
	@Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
	public Object update(@UpdateRequestBody ${classModelName} entity) {
		Result result = Result.okResult();
		${className?uncap_first}Service.update(entity);
		return result;
	}


	@ResponseBody
	@PostMapping("/delete")
	@Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
	public Object delete(@RequestBody Map<String, Object> params) {
		Result result = Result.okResult();
		String row_id =ConvertOp.convert2String(params.get("row_id"));
		${className?uncap_first}Service.delete(row_id);
		return result;
	}

}
