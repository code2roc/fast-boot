package com.code2roc.fastboot.listener;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.concurrent.TimeUnit;

@Component
public class FileListenerFactory {
    // 设置轮询间隔
    private final long interval = TimeUnit.SECONDS.toMillis(1);

    public FileAlterationMonitor getStaticResourceMonitor(String monitorDir) {
        // 装配过滤器
        FileAlterationObserver observer = new FileAlterationObserver(new File(monitorDir));
        // 向监听者添加监听器，并注入业务服务
        observer.addListener(new StaticResourceFileListener());
        // 返回监听者
        return new FileAlterationMonitor(interval, observer);
    }
}
