package com.code2roc.fastboot.util;

import com.code2roc.fastboot.model.UserTokenModel;
import com.code2roc.fastboot.system.enums.SocketEnum;
import com.code2roc.fastboot.system.org.bizlogic.UserLogic;
import com.code2roc.fastboot.system.org.model.SystemDeptDO;
import com.code2roc.fastboot.system.org.model.SystemRoleDO;
import com.code2roc.fastboot.system.org.model.SystemUserDO;
import com.code2roc.fastboot.system.org.service.ISystemDeptService;
import com.code2roc.fastboot.system.org.service.ISystemUserService;
import com.code2roc.fastboot.framework.auth.TokenUtil;
import com.code2roc.fastboot.framework.socket.SocketUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AuthUtil{
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private ISystemUserService userService;
    @Autowired
    private ISystemDeptService deptService;
    @Autowired
    private UserLogic userLogic;
    @Autowired
    private SocketUtil socketUtil;

    public UserTokenModel getUserTokenModel(){
        return tokenUtil.getTokenModel(UserTokenModel.class);
    }

    public UserTokenModel getUserTokenModel(String token){
        return tokenUtil.getTokenModel(token,UserTokenModel.class);
    }

    public void updateTokenModel(String token, UserTokenModel tokenModel) {
        tokenUtil.setTokenModel(token,tokenModel);
    }

    public String createToken(UserTokenModel tokenModel) {
       return tokenUtil.createToken(tokenModel);
    }

    public boolean checkTokenValid(String token) {
        return tokenUtil.checkTokenValid(token);
    }

    public void removeToken(String token) {
        tokenUtil.removeToken(token);
    }

    public void refreshTokenExpire(String token) {
        tokenUtil.refreshToken(token);
    }

    public void refreshTokenInfo() {
        String token = getTokenString();
        UserTokenModel userTokenModel = getUserTokenModel(token);
        SystemUserDO userDO =  userService.selectOne(userTokenModel.getUserID());
        userTokenModel.setUserID(userDO.getRow_id());
        userTokenModel.setUserName(userDO.getUser_name());
        userTokenModel.setLoginName(userDO.getLogin_name());
        SystemDeptDO deptDO = deptService.selectOne(userDO.getDept_id());
        if (deptDO != null) {
            userTokenModel.setDeptID(deptDO.getRow_id());
            userTokenModel.setDeptName(deptDO.getDept_name());
        }
        userDO = userLogic.detail(userDO.getRow_id());
        if (userDO.getRoleList() != null) {
            String roleID = "";
            String roleName = "";
            List<SystemRoleDO> roleDOList = userDO.getRoleList().stream().filter(a -> a.isRegist()).collect(Collectors.toList());
            for (SystemRoleDO roleDO : roleDOList) {
                roleName += roleDO.getRole_name() + "★";
                roleID += roleDO.getRow_id() + "★";
            }
            userTokenModel.setRoleID(roleID);
            userTokenModel.setRoleName(roleName);
        }
        updateTokenModel(token,userTokenModel);
        socketUtil.pushSocket("TokenModel变更", userTokenModel.getUserID(), SocketEnum.SocketInfo.TokenModelChange.getSocketType(), SocketEnum.SocketInfo.TokenModelChange.getSocketHandleType());
    }

    public String getTokenString() {
        return tokenUtil.getTokenString();
    }

}
