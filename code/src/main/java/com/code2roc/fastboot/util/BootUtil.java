package com.code2roc.fastboot.util;

import com.alibaba.fastjson.JSONObject;
import com.code2roc.fastboot.framework.cache.CacheUtil;
import com.code2roc.fastboot.framework.datadic.CodeItem;
import com.code2roc.fastboot.framework.datadic.DataDicManage;
import com.code2roc.fastboot.framework.util.BeanUtil;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.system.cache.DataBaseDicCacheInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BootUtil {
    public static Object convetXmlContent2FieldContent(String content, Class filedClass) throws Exception {
        Object result = null;
        if (filedClass == String.class) {
            result = ConvertOp.convert2String(content);
        } else if (filedClass == int.class || filedClass == Integer.class) {
            result = ConvertOp.convert2Int(content);
        } else if (filedClass == double.class || filedClass == Double.class) {
            result = ConvertOp.convert2Double(content);
        } else if (filedClass == Date.class) {
            result = ConvertOp.convert2Date(content);
        } else if (filedClass == boolean.class || filedClass == Boolean.class) {
            result = ConvertOp.convert2Boolean(content);
        } else if (filedClass == Map.class) {
            result = JSONObject.parseObject(content, HashMap.class);
        }
        return result;
    }

    public static String getEnumCodeText(String codeName, String codeValue) {
        String codeText = "";
        List<CodeItem> codeItemList = DataDicManage.getCodeList(codeName);
        if (codeItemList != null) {
            for (CodeItem codeItem : codeItemList) {
                if (ConvertOp.convert2String(codeItem.get_itemValue()).equals(ConvertOp.convert2String(codeValue))) {
                    codeText = codeItem.get_itemText();
                    break;
                }
            }
        }
        return codeText;
    }

    public static String getDataBaseCodeText(String codeName, String codeValue) {
        String codeText = "";
        CacheUtil cacheUtil = BeanUtil.getBean(CacheUtil.class);
        DataBaseDicCacheInfo dataBaseDicCacheInfo = (DataBaseDicCacheInfo) cacheUtil.getCache("DataBaseDic", codeName);
        List<CodeItem> codeItemList = dataBaseDicCacheInfo.getCodeItemList();
        if (codeItemList != null) {
            for (CodeItem codeItem : codeItemList) {
                if (ConvertOp.convert2String(codeItem.get_itemValue()).equals(ConvertOp.convert2String(codeValue))) {
                    codeText = codeItem.get_itemText();
                    break;
                }
            }
        }
        return codeText;
    }
}
