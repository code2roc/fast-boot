package com.code2roc.fastboot.template;

import com.code2roc.fastboot.framework.template.BaseCacheService;

public interface BaseBootCacheService<T extends BaseBootModel> extends BaseCacheService<T> {

}
