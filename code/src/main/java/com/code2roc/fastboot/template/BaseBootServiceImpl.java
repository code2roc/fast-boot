package com.code2roc.fastboot.template;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.code2roc.fastboot.framework.datasource.TargetDataSource;
import com.code2roc.fastboot.framework.template.BaseServiceImpl;
import com.code2roc.fastboot.framework.util.CommonUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
@TargetDataSource
public class BaseBootServiceImpl<T extends BaseBootModel> extends BaseServiceImpl<T> {

    @Override
    public void insert(T entity) {
        if (StringUtil.isEmpty(entity.getRow_id())) {
            entity.setRow_id(CommonUtil.getNewGuid());
        }
        entity.setGmt_create(new Date());
        entity.setGmt_modified(new Date());
        super.insert(entity);
    }

    @Override
    public void update(T entity) {
        entity.setGmt_modified(new Date());
        super.update(entity);
    }

    @Override
    public void batchInsert(List<T> entityList) {
        for(T entity:entityList){
            if (StringUtil.isEmpty(entity.getRow_id())) {
                entity.setRow_id(CommonUtil.getNewGuid());
            }
            entity.setGmt_create(new Date());
            entity.setGmt_modified(new Date());
        }
        super.batchInsert(entityList);
    }

    @Override
    public void batchUpdate(List<T> entityList) {
        for(T entity:entityList){
            entity.setGmt_modified(new Date());
        }
        super.batchUpdate(entityList);
    }

    public T selectOneByField(String fieldName, Object filedValue) {
        return (T) this.commonDTO.selectOne(this.entityClass, this.tableName, "*", fieldName, filedValue.toString());
    }

    public List<T> selectRangeList(List<String> rowIDList) {
        String rowIDString = "";
        if (rowIDList.size() == 0) {
            rowIDString = "''";
        } else {
            for (int i = 0; i < rowIDList.size(); i++) {
                if (!rowIDString.contains(rowIDList.get(i))) {
                    if (i == rowIDList.size() - 1) {
                        rowIDString += "'" + rowIDList.get(i) + "'";
                    } else {
                        rowIDString += "'" + rowIDList.get(i) + "',";
                    }
                }
            }
        }
        String sql = "  row_id in (" + rowIDString + ") ";
        return selectList("*", sql, "", null);
    }

    public List<T> selectRangeListByField(String fieldName, List<String> rowIDList) {
        String rowIDString = "";
        if (rowIDList.size() == 0) {
            rowIDString = "''";
        } else {
            for (int i = 0; i < rowIDList.size(); i++) {
                if (!rowIDString.contains(rowIDList.get(i))) {
                    if (i == rowIDList.size() - 1) {
                        rowIDString += "'" + rowIDList.get(i) + "'";
                    } else {
                        rowIDString += "'" + rowIDList.get(i) + "',";
                    }
                }
            }
        }
        String sql = "  " + fieldName + " in (" + rowIDString + ") ";
        return selectList("*", sql, "", null);
    }

    public void deleteByField(String fieldName, Object filedValue) {
        UpdateWrapper wrapper = this.commonWrapper.getDeleteWrapperCustomKey(fieldName, filedValue);
        this.getMapper().delete(wrapper);
    }

    public void deleteAll() {
        String sql = "delete from " + tableName;
        commonDTO.executeSQL(sql, null);
    }

    public boolean checkExist(String fieldName, Object fieldValue) {
        String sql = " " + fieldName + "= #{" + fieldName + "}";
        HashMap paramMap = new HashMap();
        paramMap.put(fieldName, fieldValue);
        return selectCount(sql, paramMap) > 0;
    }

    public boolean checkExist(String fieldName, Object fieldValue, String row_id) {
        String sql = " " + fieldName + "= #{" + fieldName + "} and row_id != #{row_id}";
        HashMap paramMap = new HashMap();
        paramMap.put(fieldName, fieldValue);
        paramMap.put("row_id", row_id);
        return selectCount(sql, paramMap) > 0;
    }

    public List<T> selectAllList() {
        return this.commonDTO.selectList(this.entityClass, this.tableName, "*", "1=1", "sort_num desc", null);
    }

    public List<T> selectListByField(String filedName, Object filedValue) {
        String sql = filedName + " = #{" + filedName + "}";
        HashMap paramMap = new HashMap();
        paramMap.put(filedName, filedValue);
        return selectList("*", sql, "sort_num desc", paramMap);
    }
}
