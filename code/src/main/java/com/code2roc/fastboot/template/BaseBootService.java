package com.code2roc.fastboot.template;

import com.code2roc.fastboot.framework.template.BaseService;

import java.util.List;

public interface BaseBootService<T extends BaseBootModel> extends BaseService<T> {
    T selectOneByField(String fieldName,Object filedValue);

    void deleteByField(String fieldName,Object filedValue);

    void deleteAll();

    List<T> selectAllList();

    List<T> selectListByField(String filedName,Object filedValue);

    List<T> selectRangeList(List<String> rowIDList);

    List<T> selectRangeListByField(String filedName,List<String> rowIDList);

    boolean checkExist(String fieldName,Object fieldValue);

    boolean checkExist(String fieldName,Object fieldValue,String rowID);
}
