package com.code2roc.fastboot;


import com.code2roc.fastboot.framework.file.*;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.servlet.MultipartConfigElement;

@MapperScan(basePackages = {"com.code2roc.fastboot.*.dao", "com.code2roc.fastboot.*.*.dao"})
@SpringBootApplication(scanBasePackages = {"com.code2roc"})
@ComponentScan(basePackages = {"com.code2roc"})
public class FastBootApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(FastBootApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootServletInitializer.class);
    }

    @Bean
    public IFileOperate fileOperate() {
        LocalFileStoargeProperty localFileStoargeProperty = new LocalFileStoargeProperty("attachfiles/");
        return new LocalFileOperate(localFileStoargeProperty);
    }

//    @Bean
//    public IFileOperate fileOperate() {
//        MinioFileStoargeProperty minioFileStoargeProperty = new MinioFileStoargeProperty("http://192.168.1.164:9999/","admin","abcd@1234");
//        return new MinioFileOperate(minioFileStoargeProperty);
//    }

}
