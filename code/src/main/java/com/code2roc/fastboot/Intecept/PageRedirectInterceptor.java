package com.code2roc.fastboot.Intecept;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class PageRedirectInterceptor implements HandlerInterceptor {
    @Value("${system.projectName}")
    private String projectName;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURL = request.getRequestURL().toString();
        String scheme = request.getScheme();
        String servaerName = request.getServerName();
        int port = request.getServerPort();
        String rootPageURL = scheme + ":" + "//" + servaerName + ":" + port + "/" + projectName;
        if (requestURL.equals(rootPageURL)) {
            response.sendRedirect(request.getContextPath() + "/" + projectName + "/index.html");
            return false;
        }
        return true;
    }
}