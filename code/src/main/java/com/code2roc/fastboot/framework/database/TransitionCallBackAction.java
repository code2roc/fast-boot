package com.code2roc.fastboot.framework.database;

public interface TransitionCallBackAction {
    void callback();
}
