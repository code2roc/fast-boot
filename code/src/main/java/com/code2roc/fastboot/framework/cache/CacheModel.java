package com.code2roc.fastboot.framework.cache;

public class CacheModel {
    private String cacheName;
    //Form:单表 Custom：自定义
    private String cacheType;
    private Class clazz;
    private Integer heap;
    private Integer offHeap;
    private Integer disk;

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getCacheType() {
        return cacheType;
    }

    public void setCacheType(String cacheType) {
        this.cacheType = cacheType;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Integer getHeap() {
        return heap;
    }

    public void setHeap(Integer heap) {
        this.heap = heap;
    }

    public Integer getOffHeap() {
        return offHeap;
    }

    public void setOffHeap(Integer offHeap) {
        this.offHeap = offHeap;
    }

    public Integer getDisk() {
        return disk;
    }

    public void setDisk(Integer disk) {
        this.disk = disk;
    }
}
