package com.code2roc.fastboot.framework.security;

import com.code2roc.fastboot.framework.auth.AnonymousAccess;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.template.BaseController;
import com.code2roc.fastboot.framework.util.RSAEncryptUtil;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/frame/excrypt")
@AnonymousAccess
public class EncryptController extends BaseController {

    @PostMapping("/getRSAPublickKey")
    @ResponseBody
    @AnonymousAccess
    public Object getRSAPublickKey(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String publicKey = RSAEncryptUtil.initSystemDefaultRSAKeyPair();
        result.add("obj",publicKey);
        return result;
    }
}
