package com.code2roc.fastboot.framework.quartz;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface QuartzTaskJob {
}
