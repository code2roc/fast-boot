package com.code2roc.fastboot.framework.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MailMode implements Serializable {
    private String subject;
    private String content;
    private List<String> receiverList;
    private List<MailAttachModel> attachList;

    public MailMode(){
        receiverList = new ArrayList<>();
        attachList = new ArrayList<>();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getReceiverList() {
        return receiverList;
    }

    public void setReceiverList(List<String> receiverList) {
        this.receiverList = receiverList;
    }

    public List<MailAttachModel> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<MailAttachModel> attachList) {
        this.attachList = attachList;
    }
}
