package com.code2roc.fastboot.framework.datawrapper;

import java.util.Map;

public interface BaseWrapper<T> {
    Map<String, Object> doWrap(T beWrappedModel);
}
