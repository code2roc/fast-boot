package com.code2roc.fastboot.framework.message;

import com.alibaba.fastjson.JSON;
import com.code2roc.fastboot.framework.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "system.messageChannel", havingValue = "redis", matchIfMissing = true)
public class RedisMessageReceiver {
    @Autowired
    private ChannelConfig channelConfig;

    public void receiveMessage(String message) {
        MessageModel messageModel = JSON.parseObject(message, MessageModel.class);
        if(messageModel.isGlobalMessage()){
            //全局消息处理排除消息发送者
            if(!channelConfig.getChannelName().equals(messageModel.getSendFromSystemChannel())){
                System.out.println("处理全局消息，来源："+ messageModel.getSendFromSystemChannel());
                IMessageReceiver receiver = BeanUtil.getBean(messageModel.getHandleClazz());
                receiver.handleMessage(JSON.parseObject(messageModel.getBodyContent(), messageModel.getBodyClass()), messageModel.getExtraParam());
            }
        }else{
            IMessageReceiver receiver = BeanUtil.getBean(messageModel.getHandleClazz());
            receiver.handleMessage(JSON.parseObject(messageModel.getBodyContent(), messageModel.getBodyClass()), messageModel.getExtraParam());
        }
    }
}
