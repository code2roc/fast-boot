package com.code2roc.fastboot.framework.quartz;

import org.quartz.*;

import java.io.Serializable;
import java.util.Date;

public class CustomerJobExecutionContext implements Serializable, JobExecutionContext {
    private transient Scheduler scheduler;
    private String jobName;

    public CustomerJobExecutionContext(Scheduler scheduler,String jobName){
        this.scheduler = scheduler;
        this.jobName = jobName;
    }

    @Override
    public Scheduler getScheduler() {
        return scheduler;
    }

    @Override
    public Trigger getTrigger() {
        return null;
    }

    @Override
    public Calendar getCalendar() {
        return null;
    }

    @Override
    public boolean isRecovering() {
        return false;
    }

    @Override
    public TriggerKey getRecoveringTriggerKey() throws IllegalStateException {
        return null;
    }

    @Override
    public int getRefireCount() {
        return 0;
    }

    @Override
    public JobDataMap getMergedJobDataMap() {
        return null;
    }

    @Override
    public JobDetail getJobDetail() {
        return new JobDetail() {
            @Override
            public JobKey getKey() {
                return new JobKey(jobName);
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public Class<? extends Job> getJobClass() {
                return null;
            }

            @Override
            public JobDataMap getJobDataMap() {
                return null;
            }

            @Override
            public boolean isDurable() {
                return false;
            }

            @Override
            public boolean isPersistJobDataAfterExecution() {
                return false;
            }

            @Override
            public boolean isConcurrentExectionDisallowed() {
                return false;
            }

            @Override
            public boolean requestsRecovery() {
                return false;
            }

            @Override
            public JobBuilder getJobBuilder() {
                return null;
            }

            @Override
            public Object clone() {
                return null;
            }
        };
    }

    @Override
    public Job getJobInstance() {
        return null;
    }

    @Override
    public Date getFireTime() {
        return null;
    }

    @Override
    public Date getScheduledFireTime() {
        return null;
    }

    @Override
    public Date getPreviousFireTime() {
        return null;
    }

    @Override
    public Date getNextFireTime() {
        return null;
    }

    @Override
    public String getFireInstanceId() {
        return null;
    }

    @Override
    public Object getResult() {
        return null;
    }

    @Override
    public void setResult(Object o) {

    }

    @Override
    public long getJobRunTime() {
        return 0;
    }

    @Override
    public void put(Object o, Object o1) {

    }

    @Override
    public Object get(Object o) {
        return null;
    }
}
