package com.code2roc.fastboot.framework.permission;

public interface IPermissionAuth {
    boolean checkDataAccess();
}
