package com.code2roc.fastboot.framework.cache;

import com.alibaba.fastjson.JSON;
import com.code2roc.fastboot.framework.global.GlobalEnum;
import com.code2roc.fastboot.framework.redis.RedisUtil;
import com.code2roc.fastboot.framework.template.BaseSystemObject;
import com.code2roc.fastboot.framework.util.BeanUtil;
import com.code2roc.fastboot.framework.util.ConvertOp;
import org.ehcache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class CacheExecuteUtil {
    @Autowired
    private RedisUtil redisUtil;

    public void putCacheIntoTransition(){
        String threadID = Thread.currentThread().getName();
        System.out.println("init threadid:"+threadID);
        CacheExecuteModel cacheExecuteModel = new CacheExecuteModel();
        cacheExecuteModel.setExecuteType("option");
        redisUtil.redisTemplateSetForCollection(threadID,cacheExecuteModel, GlobalEnum.RedisDBNum.Cache.get_value());
        redisUtil.setExpire(threadID,5, TimeUnit.MINUTES, GlobalEnum.RedisDBNum.Cache.get_value());
    }

    public void putCache(String cacheName, String key, BaseSystemObject value) {
        if(checkCacheOptinionInTransition()){
            String threadID = Thread.currentThread().getName();
            CacheExecuteModel cacheExecuteModel = new CacheExecuteModel("update", cacheName, key, value.getClass().getName(),value);
            redisUtil.redisTemplateSetForCollection(threadID,cacheExecuteModel, GlobalEnum.RedisDBNum.Cache.get_value());
            redisUtil.setExpire(threadID,5, TimeUnit.MINUTES, GlobalEnum.RedisDBNum.Cache.get_value());
        }else{
            executeUpdateOperation(cacheName,key,value);
        }

    }

    public void deleteCache(String cacheName, String key) {
        if(checkCacheOptinionInTransition()){
            String threadID = Thread.currentThread().getName();
            CacheExecuteModel cacheExecuteModel = new CacheExecuteModel("delete", cacheName, key);
            redisUtil.redisTemplateSetForCollection(threadID,cacheExecuteModel, GlobalEnum.RedisDBNum.Cache.get_value());
            redisUtil.setExpire(threadID,5, TimeUnit.MINUTES, GlobalEnum.RedisDBNum.Cache.get_value());
        }else{
            executeDeleteOperation(cacheName,key);
        }
    }

    public void clearCache(String cacheName) {
        if(checkCacheOptinionInTransition()){
            String threadID = Thread.currentThread().getName();
            CacheExecuteModel cacheExecuteModel = new CacheExecuteModel("clear", cacheName);
            redisUtil.redisTemplateSetForCollection(threadID,cacheExecuteModel, GlobalEnum.RedisDBNum.Cache.get_value());
            redisUtil.setExpire(threadID,5, TimeUnit.MINUTES, GlobalEnum.RedisDBNum.Cache.get_value());
        }else{
            executeClearOperation(cacheName);
        }
    }

    public void executeOperation(){
        String threadID = Thread.currentThread().getName();
        if(checkCacheOptinionInTransition()){
            List<LinkedHashMap> executeList =  redisUtil.redisTemplateGetForCollectionAll(threadID, GlobalEnum.RedisDBNum.Cache.get_value());
            Collections.reverse(executeList);
            for (LinkedHashMap obj:executeList) {
                String executeType = ConvertOp.convert2String(obj.get("executeType"));
                if(executeType.contains("option")){
                    continue;
                }
                String obejctClazzName = ConvertOp.convert2String(obj.get("obejctClazzName"));
                String cacheName = ConvertOp.convert2String(obj.get("cacheName"));
                String key = ConvertOp.convert2String(obj.get("key"));
                LinkedHashMap valueMap = (LinkedHashMap)obj.get("value");
                String valueMapJson =  JSON.toJSONString(valueMap);
                try{
                    if(executeType.equals("update")){
                        Object valueInstance = JSON.parseObject(valueMapJson,Class.forName(obejctClazzName));
                        executeUpdateOperation(cacheName,key,(BaseSystemObject)valueInstance);
                    }else if(executeType.equals("delete")){
                        executeDeleteOperation(cacheName,key);
                    }else if(executeType.equals("clear")){
                        executeClearOperation(cacheName);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            redisUtil.redisTemplateRemove(threadID,GlobalEnum.RedisDBNum.Cache.get_value());
        }

    }

    private void executeUpdateOperation(String cacheName, String key, BaseSystemObject value) {
        EhcacheConfig ehcacheConfig = BeanUtil.getBean(EhcacheConfig.class);
        Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
        if (cache.containsKey(key)) {
            cache.replace(key, value);
        } else {
            cache.put(key, value);
        }
    }

    private void executeDeleteOperation(String cacheName, String key) {
        EhcacheConfig ehcacheConfig = BeanUtil.getBean(EhcacheConfig.class);
        Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
        cache.remove(key);
    }

    private void executeClearOperation(String cacheName) {
        EhcacheConfig ehcacheConfig = BeanUtil.getBean(EhcacheConfig.class);
        Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
        cache.clear();
    }

    private boolean checkCacheOptinionInTransition(){
        RedisUtil redisUtil = BeanUtil.getBean(RedisUtil.class);
        String threadID = Thread.currentThread().getName();
        System.out.println("check threadid:"+threadID);
        return redisUtil.isValid(threadID, GlobalEnum.RedisDBNum.Cache.get_value());
    }
}
