package com.code2roc.fastboot.framework.socket;

import com.code2roc.fastboot.framework.database.TransactionCallBackUtil;
import com.code2roc.fastboot.framework.model.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SocketUtil {
    @Autowired
    private TransactionCallBackUtil transactionCallBackUtil;

    public void pushSocket(String socketMessage, String userID, String socketType, String socketHandle) {
        transactionCallBackUtil.execute(() -> {
            Result result = Result.okResult();
            result.setMsg(socketMessage);
            result.add("socketType", socketType);
            result.add("socketHandle", socketHandle);
            BaseWebSocketServer.sendMessageToUser(userID, result);
            SSESocketManage.sendMessageToUser(userID, result);
        });
    }

    public void pushSocket(String socketMessage, String userID, String socketType, String socketHandle, HashMap<String, Object> extraData) {
        transactionCallBackUtil.execute(() -> {
            Result result = Result.okResult();
            result.setMsg(socketMessage);
            result.add("socketType", socketType);
            result.add("socketHandle", socketHandle);
            if (extraData != null) {
                for (Map.Entry<String, Object> entity : extraData.entrySet()) {
                    result.add(entity.getKey(), entity.getValue());
                }
            }
            BaseWebSocketServer.sendMessageToUser(userID, result);
            SSESocketManage.sendMessageToUser(userID, result);
        });
    }
}
