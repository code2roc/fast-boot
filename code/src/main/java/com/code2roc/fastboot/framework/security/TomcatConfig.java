package com.code2roc.fastboot.framework.security;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.Ssl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class TomcatConfig {
    @Autowired
    private SecurityConfig securityConfig;
    @Value("${server.port}")
    private int serverPort;

    @Bean
    @ConditionalOnProperty(name = "system.package", havingValue = "jar", matchIfMissing = true)
    public TomcatServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcatServletContainerFactory = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint constraint = new SecurityConstraint();
                SecurityCollection collection = new SecurityCollection();
                if(securityConfig.isOpenSSL()){
                    constraint.setUserConstraint("CONFIDENTIAL");
                }
                //http方法
                List<String> forbiddenList = new ArrayList<>();
                if(securityConfig.isLimitHTTPMethod()){
                    forbiddenList = Arrays.asList("PUT|DELETE|HEAD|TRACE".split("\\|"));
                }
                for (String method : forbiddenList) {
                    collection.addMethod(method);
                }
                //url匹配表达式
                collection.addPattern("/*");
                constraint.addCollection(collection);
                context.addConstraint(constraint);
            }
        };
        if(securityConfig.isOpenSSL()){
            Ssl ssl = new Ssl();
            ssl.setKeyStore(securityConfig.getSslStore());
            ssl.setKeyPassword(securityConfig.getSslStorePassword());
            ssl.setKeyStoreType(securityConfig.getSslStoreType());
            ssl.setKeyAlias(securityConfig.getSslalias());
            tomcatServletContainerFactory.setSsl(ssl);
            if(securityConfig.getSslNeedRedirectPort()!=0){
                tomcatServletContainerFactory.addAdditionalTomcatConnectors(createHTTPConnector());
            }
        }
        tomcatServletContainerFactory.addConnectorCustomizers(new FrameTomcatConnectorCustomizer());
        return tomcatServletContainerFactory;
    }

    private Connector createHTTPConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        connector.setSecure(false);
        connector.setPort(securityConfig.getSslNeedRedirectPort());
        connector.setRedirectPort(serverPort);
        return connector;
    }

    class FrameTomcatConnectorCustomizer implements TomcatConnectorCustomizer {
        @Override
        public void customize(Connector connector) {
            Http11NioProtocol protocal=(Http11NioProtocol)connector.getProtocolHandler();
            protocal.setMaxConnections(20000);//最大连接数
            protocal.setMaxThreads(500);//最大线程数
            protocal.setMaxHttpHeaderSize(20480000);//请求头大小
        }
    }
}
