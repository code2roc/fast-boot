package com.code2roc.fastboot.framework.template;

import java.util.List;

public interface BaseCacheService<T extends BaseModel> {
    void insert(T entity);

    void delete(String unitguid);

    void batchDelte(List<String> unitguidList);

    void update(T entity);

    void batchUpdate(List<T> entityList);

    T selectOne(String unitguid);

    List<T> selectAllList();

    void clear();
}
