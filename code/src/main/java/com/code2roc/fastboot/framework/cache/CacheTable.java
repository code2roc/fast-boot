package com.code2roc.fastboot.framework.cache;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CacheTable {
    String heap() default "";
    String offheap() default "";
    String disk() default "";
}
