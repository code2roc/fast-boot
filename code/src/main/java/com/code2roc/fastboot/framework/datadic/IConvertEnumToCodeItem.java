package com.code2roc.fastboot.framework.datadic;

public interface IConvertEnumToCodeItem {
    String getCodeName();
}
