package com.code2roc.fastboot.framework.global;

public class GlobalEnum {
    public enum RedisDBNum {
        CheckLock(1, "锁定检查"), ValidateCode(2, "验证码"), Cache(3, "缓存");
        private int _value;
        private String _name;

        private RedisDBNum(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }
    }
}
