package com.code2roc.fastboot.framework.global;

import com.alibaba.fastjson.serializer.ValueFilter;
import com.code2roc.fastboot.framework.util.FastJsonUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FastJsonPropertyValueFilter implements ValueFilter {
    @Override
    public Object process(Object source, String name, Object value) {
        String key = source.getClass().getName() + "|" + name;
        if (value != null && FastJsonUtil.checkDate2Json(key)) {
            String format = FastJsonUtil.getDate2JsonFormat(key);
            DateFormat df = new SimpleDateFormat(format);
            return df.format(value);
        }
        return value;
    }
}
