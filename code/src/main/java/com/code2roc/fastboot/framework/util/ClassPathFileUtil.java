package com.code2roc.fastboot.framework.util;

import com.code2roc.fastboot.framework.global.SystemConfig;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ClassPathFileUtil {
    //pom文件必须配置<exclude>systemfile/*</exclude>和<include>systemfile/*</include>，且文件要创建在systemfile下
    public static String getFilePath(String classFilePath) {
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        String filePath = "";
        try {
            String templateFilePath = systemConfig.getTemplatePath() + "classpathfile/";
            File tempDir = new File(templateFilePath);
            if (!tempDir.exists()) {
                tempDir.mkdirs();
            }
            String[] filePathList = classFilePath.split("/");
            String checkFilePath = systemConfig.getTemplatePath() + "classpathfile";
            for (String item : filePathList) {
                checkFilePath += "/" + item;
            }
            File tempFile = new File(checkFilePath);
            if (tempFile.exists()) {
                filePath = checkFilePath;
            } else {
                //解析
                ClassPathResource classPathResource = new ClassPathResource(classFilePath);
                InputStream inputStream = classPathResource.getInputStream();
                checkFilePath = systemConfig.getTemplatePath() + "classpathfile";
                for (int i = 0; i < filePathList.length; i++) {
                    checkFilePath += "/" + filePathList[i];
                    if (i == filePathList.length - 1) {
                        //文件
                        File file = new File(checkFilePath);
                        if (!file.exists()) {
                            FileUtils.copyInputStreamToFile(inputStream, file);
                        }
                    } else {
                        //目录
                        tempDir = new File(checkFilePath);
                        if (!tempDir.exists()) {
                            tempDir.mkdirs();
                        }
                    }
                }
                inputStream.close();
                filePath = checkFilePath;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }

    public static List<String> getFolderFilePathList(String folderPath, String fileExt) {
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        List<String> filePathResultList = new ArrayList<>();
        String dirPath = "";
        try {
            String templateFilePath = systemConfig.getTemplatePath() + "classpathfile/";
            File tempDir = new File(templateFilePath);
            if (!tempDir.exists()) {
                tempDir.mkdirs();
            }
            String[] filePathList = folderPath.split("/");
            String checkFilePath = systemConfig.getTemplatePath() + "classpathfile";
            for (String item : filePathList) {
                checkFilePath += "/" + item;
            }
            //创建文件夹
            File tempFile = new File(checkFilePath);
            if (tempFile.exists()) {
                dirPath = checkFilePath;
            } else {
                checkFilePath = systemConfig.getTemplatePath() + "classpathfile";
                for (int i = 0; i < filePathList.length; i++) {
                    checkFilePath += "/" + filePathList[i];
                    tempDir = new File(checkFilePath);
                    if (!tempDir.exists()) {
                        tempDir.mkdirs();
                    }
                }
                dirPath = checkFilePath;
            }

            //复制文件夹下所有文件
            if (!folderPath.endsWith("/")) {
                folderPath = folderPath + "/";
            }
            Resource[] resources = new PathMatchingResourcePatternResolver().getResources(folderPath + "*" + fileExt);
            for (int i = 0; i < resources.length; i++) {
                InputStream inputStream = resources[i].getInputStream();
                String filePath = dirPath + "/" + resources[i].getFilename();
                File file = new File(filePath);
                if (!file.exists()) {
                    FileUtils.copyInputStreamToFile(inputStream, file);
                }
                filePathResultList.add(filePath);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePathResultList;
    }

    public static void clearClassPathFileTemplateFolder() {
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        String templateFilePath = systemConfig.getTemplatePath() + "classpathfile/";
        File tempDir = new File(templateFilePath);
        FileSystemUtils.deleteRecursively(tempDir);
        System.out.println("清除classpathfile临时文件成功");
    }
}
