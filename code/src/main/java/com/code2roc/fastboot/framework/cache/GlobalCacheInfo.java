package com.code2roc.fastboot.framework.cache;

import com.code2roc.fastboot.framework.template.BaseSystemObject;

public class GlobalCacheInfo extends BaseSystemObject {
    private String methodName;
    private Class[] paramClassList;
    private Object[] paramList;
    private String className;

    public GlobalCacheInfo(){

    }

    public GlobalCacheInfo(String methodName,Class[] paramClassList,Object[] paramList){
        this.methodName = methodName;
        this.paramClassList = paramClassList;
        this.paramList = paramList;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getParamList() {
        return paramList;
    }

    public void setParamList(Object[] paramList) {
        this.paramList = paramList;
    }

    public Class[] getParamClassList() {
        return paramClassList;
    }

    public void setParamClassList(Class[] paramClassList) {
        this.paramClassList = paramClassList;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}

