package com.code2roc.fastboot.framework.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static String getDateTagToDay() {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String tag = df.format(new Date());
        return tag;
    }

    public static String getDateTagToDay(Date date) {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String tag = df.format(date);
        return tag;
    }

    public static String getDateTagToSecond() {
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String tag = df.format(new Date());
        return tag;
    }

    public static String getDateTagToSecond(Date date) {
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String tag = df.format(date);
        return tag;
    }

    public static String getDateFormatToSecond() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String tag = df.format(new Date());
        return tag;
    }

    public static String getDateFormatToMillSecond() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String tag = df.format(new Date());
        return tag;
    }

    public static String getDateFormatToSecond(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String tag = df.format(date);
        return tag;
    }

    public static String getDateFormatToMillSecond(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String tag = df.format(date);
        return tag;
    }

    public static long getTimePeroidSecond(Date startDate, Date endDate) {
        return (endDate.getTime() - (startDate.getTime())) / (1000);
    }

    public static long getTimePeroidToNowSecond(Date startDate) {
        return (new Date().getTime() - (startDate.getTime())) / (1000);
    }

    public static long getTimePeroidMillSecond(Date startDate, Date endDate) {
        return endDate.getTime() - startDate.getTime();
    }

    public static long getTimePeroidToNowMillSecond(Date startDate) {
        return new Date().getTime() - startDate.getTime();
    }
}
