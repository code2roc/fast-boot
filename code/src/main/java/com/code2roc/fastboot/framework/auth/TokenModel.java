package com.code2roc.fastboot.framework.auth;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TokenModel implements Serializable {
    private String userID;
    private String userName;
    private String loginName;
    private Boolean autoRefreshExpire;
    private HashMap<String,String> extraMap;
    private HashMap<String, String> threadMap;
    public TokenModel() {
        extraMap = new HashMap<>();
        threadMap = new HashMap<>();
        autoRefreshExpire = true;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public HashMap<String, String> getExtraMap() {
        return extraMap;
    }

    public void setExtraMap(HashMap<String, String> extraMap) {
        this.extraMap = extraMap;
    }

    public void addExtra(String key, String value) {
        extraMap.put(key, value);
    }

    public String getExtra(String key) {
        return extraMap.get(key);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Boolean getAutoRefreshExpire() {
        return autoRefreshExpire;
    }

    public void setAutoRefreshExpire(Boolean autoRefreshExpire) {
        this.autoRefreshExpire = autoRefreshExpire;
    }

    public HashMap<String, String> getThreadMap() {
        return threadMap;
    }

    public void setThreadMap(HashMap<String, String> threadMap) {
        this.threadMap = threadMap;
    }

    public void addThread(String threadName) {
        if (threadMap == null) {
            threadMap = new HashMap<>();
        }
        threadMap.put(threadName, "1");
    }

    public void addThread(String key, String value) {
        if (threadMap == null) {
            threadMap = new HashMap<>();
        }
        threadMap.put(key, value);
    }

    public void removeThread(String threadName) {
        if (threadMap.containsKey(threadName)) {
            threadMap.remove(threadName);
        }
    }

    public List<String> getThreadList() {
        return new ArrayList<>(threadMap.keySet());
    }
}
