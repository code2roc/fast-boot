package com.code2roc.fastboot.framework.global;

import com.code2roc.fastboot.framework.auth.AnonymousAccess;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.template.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/frame/error")
@AnonymousAccess
public class ErrorController extends BaseController {
    @ResponseBody
    @RequestMapping("/tokenAuthError")
    public Object tokenAuthError(){
        Result result = Result.errorResult();
        result.setCode(SystemErrorCodeEnum.ErrorCode.TokenAuthError.get_value());
        result.setMsg(SystemErrorCodeEnum.ErrorCode.TokenAuthError.get_name());
        return result;
    }

    @ResponseBody
    @RequestMapping("/forbiddenVisitError")
    public Object forbiddenVisitError(){
        Result result = Result.errorResult();
        result.setCode(SystemErrorCodeEnum.ErrorCode.NoAuthAccess.get_value());
        result.setMsg(SystemErrorCodeEnum.ErrorCode.NoAuthAccess.get_name());
        return result;
    }


    @ResponseBody
    @RequestMapping("/sqlInjectionError")
    public Object sqlInjectionError(){
        Result result = Result.errorResult();
        result.setCode(SystemErrorCodeEnum.ErrorCode.SqlInjectionError.get_value());
        result.setMsg(SystemErrorCodeEnum.ErrorCode.SqlInjectionError.get_name());
        return result;
    }

    @ResponseBody
    @RequestMapping("/404")
    public Object error404(){
        Result result = Result.errorResult();
        result.setCode(SystemErrorCodeEnum.ErrorCode.NotFoundError.get_value());
        result.setMsg(SystemErrorCodeEnum.ErrorCode.NotFoundError.get_name());
        return result;
    }
}
