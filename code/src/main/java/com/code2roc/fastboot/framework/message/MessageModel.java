package com.code2roc.fastboot.framework.message;

import java.util.HashMap;

public class MessageModel {
    private Class<? extends IMessageReceiver> handleClazz;
    private String bodyContent;
    private Class bodyClass;
    private HashMap extraParam;
    private boolean globalMessage;
    private String sendFromSystemChannel;

    public MessageModel(){
        globalMessage = false;
        extraParam = new HashMap();
    }

    public Class<? extends IMessageReceiver> getHandleClazz() {
        return handleClazz;
    }

    public void setHandleClazz(Class<? extends IMessageReceiver> handleClazz) {
        this.handleClazz = handleClazz;
    }

    public HashMap getExtraParam() {
        return extraParam;
    }

    public void setExtraParam(HashMap extraParam) {
        this.extraParam = extraParam;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public void setBodyContent(String bodyContent) {
        this.bodyContent = bodyContent;
    }

    public Class getBodyClass() {
        return bodyClass;
    }

    public void setBodyClass(Class bodyClass) {
        this.bodyClass = bodyClass;
    }

    public void setGlobalMessage(boolean globalMessage) {
        this.globalMessage = globalMessage;
    }

    public boolean isGlobalMessage() {
        return globalMessage;
    }

    public String getSendFromSystemChannel() {
        return sendFromSystemChannel;
    }

    public void setSendFromSystemChannel(String sendFromSystemChannel) {
        this.sendFromSystemChannel = sendFromSystemChannel;
    }
}
