package com.code2roc.fastboot.framework.auth;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
@Inherited
public @interface AnonymousAccess {
}