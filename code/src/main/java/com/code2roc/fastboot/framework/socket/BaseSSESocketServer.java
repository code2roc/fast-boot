package com.code2roc.fastboot.framework.socket;

import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.template.BaseController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/frame/sse/")
public class BaseSSESocketServer extends BaseController {

    //创建SSE长链接
    @GetMapping("/createConnect")
    public SseEmitter createConnect(HttpServletRequest request) {
        String token = request.getParameter("token");
        String clientID = request.getParameter("clientID");
        return SSESocketManage.createConnect(token,clientID);
    }

    //关闭SSE连接
    @GetMapping("/closeConnect")
    @ResponseBody
    public Object closeConnect(HttpServletRequest request) {
        String token = request.getParameter("token");
        String clientID = request.getParameter("clientID");
        SSESocketManage.closeConnect(token,clientID);
        return Result.okResult();
    }

}
