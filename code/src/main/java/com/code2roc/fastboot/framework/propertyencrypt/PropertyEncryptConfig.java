package com.code2roc.fastboot.framework.propertyencrypt;

import com.ulisesbocchio.jasyptspringboot.EncryptablePropertyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class PropertyEncryptConfig {
    @Bean(name="encryptablePropertyResolver")
    EncryptablePropertyResolver encryptablePropertyResolver(){
        return new CustomEncryptPropertyResolver();
    }
}
