package com.code2roc.fastboot.framework.message;

import com.code2roc.fastboot.framework.global.SystemConfig;
import com.code2roc.fastboot.framework.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ChannelConfig {
    @Autowired
    private SystemConfig systemConfig;
    @Value("${spring.profiles.active}")
    private String active;

    public String getChannelName() {
        return systemConfig.getProjectName() + "-" + active + "-" + CommonUtil.getMacAddress();
    }

    public String getGlobalChannelName() {
        return systemConfig.getProjectName() + "-" + active;
    }
}
