package com.code2roc.fastboot.framework.global;

import com.alibaba.fastjson.serializer.NameFilter;

public class FastJsonPropertyNameFilter implements NameFilter {
    @Override
    public String process(Object source, String name, Object value) {
        name = name.substring(0,1).toLowerCase()+ name.substring(1,name.length());
        return name;
    }
}