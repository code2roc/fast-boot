package com.code2roc.fastboot.framework.mock;

import com.code2roc.fastboot.framework.auth.TokenModel;

public interface IMockIdentify {
    TokenModel getMockTokenModel(String loginName);
}
