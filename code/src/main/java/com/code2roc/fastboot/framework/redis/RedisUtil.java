package com.code2roc.fastboot.framework.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {
    @Autowired
    private RedisManage redisManage;
    private int defatuDBIndex = 0;

    public void redisTemplateSet(String key, Object value) {
        ValueOperations<Object, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForValue();
        ops.set(key, value);
    }

    public void redisTemplateSet(String key, Object value, int dbIndex) {
        ValueOperations<Object, Object> ops = redisManage.redisTemplate(dbIndex).opsForValue();
        ops.set(key, value);
    }

    public Object redisTemplateGet(String key) {
        ValueOperations<Object, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForValue();
        return ops.get(key);
    }

    public Object redisTemplateGet(String key, int dbIndex) {
        ValueOperations<Object, Object> ops = redisManage.redisTemplate(dbIndex).opsForValue();
        return ops.get(key);
    }

    public void redisTemplateRemove(String key) {
        ValueOperations<Object, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForValue();
        ops.getOperations().delete(key);
    }

    public void redisTemplateRemove(String key, int dbIndex) {
        ValueOperations<Object, Object> ops = redisManage.redisTemplate(dbIndex).opsForValue();
        ops.getOperations().delete(key);
    }

    public void redisTemplateSetForList(String key, Map<String, Object> map) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForHash();
        ops.putAll(key, map);
    }

    public void redisTemplateSetForList(String key, Map<String, Object> map, int dbIndex) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(dbIndex).opsForHash();
        ops.putAll(key, map);
    }

    public void redisTemplateSetForList(String key, String hashKey, Object value) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForHash();
        ops.put(key, hashKey, value);
    }

    public void redisTemplateSetForList(String key, String hashKey, Object value, int dbIndex) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(dbIndex).opsForHash();
        ops.put(key, hashKey, value);
    }

    public Map<Object, Object> redisTemplateGetForList(String key) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForHash();
        return ops.entries(key);
    }

    public Map<Object, Object> redisTemplateGetForList(String key, int dbIndex) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(dbIndex).opsForHash();
        return ops.entries(key);
    }

    public Object redisTemplateGetForList(String key, String hasKey) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForHash();
        return ops.get(key, hasKey);
    }

    public Object redisTemplateGetForList(String key, String hasKey, int dbIndex) {
        HashOperations<Object, Object, Object> ops = redisManage.redisTemplate(dbIndex).opsForHash();
        return ops.get(key, hasKey);
    }


    public void redisTemplateSetForCollection(String key, Object value) {
        ListOperations<String, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForList();
        ops.leftPush(key, value);
    }

    public void redisTemplateSetForCollection(String key, Object value, int dbIndex) {
        ListOperations<String, Object> ops = redisManage.redisTemplate(dbIndex).opsForList();
        ops.leftPush(key, value);
    }

    public Object redisTemplateGetForCollection(String key, Object value) {
        ListOperations<String, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForList();
        return ops.rightPop(key);
    }

    public Object redisTemplateGetForCollection(String key, int dbIndex) {
        ListOperations<String, Object> ops = redisManage.redisTemplate(dbIndex).opsForList();
        return ops.rightPop(key);
    }

    public long redisTemplateGetForCollectionSize(String key, Object value) {
        ListOperations<String, Object> ops = redisManage.redisTemplate(defatuDBIndex).opsForList();
        return ops.size(key);
    }

    public long redisTemplateGetForCollectionSize(String key, Object value, int dbIndex) {
        ListOperations<String, Object> ops = redisManage.redisTemplate(dbIndex).opsForList();
        return ops.size(key);
    }

    public List<LinkedHashMap> redisTemplateGetForCollectionAll(String key, Object value) {
        ListOperations<String, LinkedHashMap> ops = redisManage.redisTemplate(defatuDBIndex).opsForList();
        long size = ops.size(key);
        return ops.range(key, 0, size - 1);
    }

    public List<LinkedHashMap> redisTemplateGetForCollectionAll(String key, int dbIndex) {
        ListOperations<String, LinkedHashMap> ops = redisManage.redisTemplate(dbIndex).opsForList();
        long size = ops.size(key);
        return ops.range(key, 0, size - 1);
    }

    public void setExpire(String key, long timeout, final TimeUnit unit, int dbIndex) {
        redisManage.redisTemplate(dbIndex).expire(key, timeout, unit);
    }

    public void setExpire(String key, long timeout, final TimeUnit unit) {
        redisManage.redisTemplate(defatuDBIndex).expire(key, timeout, unit);
    }


    public boolean isValid(String key) {
        boolean flag = false;
        if (redisManage.redisTemplate(defatuDBIndex).hasKey(key)) {
            if (redisManage.redisTemplate(defatuDBIndex).getExpire(key, TimeUnit.SECONDS) > 0) {
                flag = true;
            }
        }
        return flag;
    }

    public boolean isValid(String key, int dbIndex) {
        boolean flag = false;
        if (redisManage.redisTemplate(dbIndex).hasKey(key)) {
            if (redisManage.redisTemplate(dbIndex).getExpire(key, TimeUnit.SECONDS) > 0) {
                flag = true;
            }
        }
        return flag;
    }

    public boolean isExist(String key, int dbIndex) {
        boolean flag = false;
        if (redisManage.redisTemplate(dbIndex).hasKey(key)) {
            flag = true;
        }
        return flag;
    }

    public void sendMessage(String channel, Object message) {
        redisManage.redisTemplate(defatuDBIndex).convertAndSend(channel, message);
    }

    public void sendMessage(String channel, Object message, int dbIndex) {
        redisManage.redisTemplate(dbIndex).convertAndSend(channel, message);
    }
}
