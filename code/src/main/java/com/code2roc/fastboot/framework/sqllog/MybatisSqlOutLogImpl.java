package com.code2roc.fastboot.framework.sqllog;

import com.code2roc.fastboot.framework.util.StringUtil;
import org.apache.ibatis.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MybatisSqlOutLogImpl implements Log {
    private Logger log;

    public MybatisSqlOutLogImpl(String clazz){
        log = LoggerFactory.getLogger(clazz);
    }

    public boolean isDebugEnabled() {
        return StringUtil.isEmpty(SqlLogContextHolder.getTag());
    }

    public boolean isTraceEnabled() {
        return this.log.isTraceEnabled();
    }

    public void error(String s, Throwable e) {
        this.log.error(s, e);
    }

    public void error(String s) {
        this.log.error(s);
    }

    public void debug(String s) {
        this.log.debug(s);
    }

    public void trace(String s) {
        this.log.trace(s);
    }

    public void warn(String s) {
        this.log.warn(s);
    }
}
