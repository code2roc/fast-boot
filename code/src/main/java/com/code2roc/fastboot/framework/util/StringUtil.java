package com.code2roc.fastboot.framework.util;

import org.apache.commons.lang3.text.StrBuilder;

import java.io.File;
import java.util.regex.Pattern;

public class StringUtil {
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str) || "null".equals(str);
    }

    public static String padLeft(String src, int len, char ch) {
        int diff = len - src.length();
        if (diff <= 0) {
            return src;
        }

        char[] charr = new char[len];
        System.arraycopy(src.toCharArray(), 0, charr, diff, src.length());
        for (int i = 0; i < diff; i++) {
            charr[i] = ch;
        }
        return new String(charr);
    }

    public static String uncapitalize(String str) {
        int strLen;
        return str != null && (strLen = str.length()) != 0 ? (new StrBuilder(strLen)).append(Character.toLowerCase(str.charAt(0))).append(str.substring(1)).toString() : str;
    }

    public static String getConverPath(String path) {
        return path.replaceAll("\\.", "\\\\") + File.separator;

    }

    //通过正则表达式判断是否是英文字母和数字加下划线和中文
    public static boolean checkIsOnlyContainCharAndNum(String content) {
        content = content.replace("_", "");
        String regex = "^[a-zA-Z0-9_-\u4e00-\u9fa5]*$";
        return Pattern.matches(regex, content);
    }

    //判断关键字后面是否是某个特定的字符
    public static boolean checkFlowChar(String content, String keyword, String followchar, boolean CheckPostfixIsEmpty) {
        int index = content.indexOf(keyword);
        while (index >= 0) {
            if ((index + keyword.length()) >= content.length()) // 如果关键字是最后一个字，没有后续字符
                return false;

            if ((index + keyword.length() + followchar.length()) > content.length()) // 如果关键字后面的字符串不够后续字符串程度
                return false;

            String followchar1 = subString(content,index + keyword.length(), followchar.length());

            if (followchar.equals(followchar1)) {
                if (CheckPostfixIsEmpty) {
                    if (index == 0)// 第一个字符就是关键字
                        return true;
                    else if (subString(content,index - 1, 1) == " ") // 关键字前面是
                        return true;
                    else if (subString(content,index - CommonUtil.getNewLine().length(), CommonUtil.getNewLine().length()) == CommonUtil.getNewLine()) // 关键字前面是回车
                        return true;
                } else {
                    return true;
                }
            }

            index = content.indexOf(keyword, index + keyword.length());
        }
        return false;
    }

    //判断Key前面和后面是否是英文字符
    public static boolean checkBeforAfterChar(String Source, String Key) {
        if (StringUtil.isEmpty(Source) || StringUtil.isEmpty(Key))
            return true;
        int index = Source.indexOf(Key);
        while (index >= 0) {
            if (index == 0) {
                if (!checkIsEnglishChar(Source.toCharArray()[Key.length()])) {
                    return false;
                }
            } else if (index == Source.length() - Key.length()) {
                if (!checkIsEnglishChar(Source.toCharArray()[index - 1])) {
                    return false;
                }
            } else {
                if (!checkIsEnglishChar(Source.toCharArray()[index - 1]) || !checkIsEnglishChar(Source.toCharArray()[index + Key.length()])) {
                    return false;
                }
            }
            Source = Source.substring(index + Key.length());
            index = Source.indexOf(Key);
        }
        return true;
    }

    //判断是否是英文字母
    public static boolean checkIsEnglishChar(char c) {
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
            return true;
        else
            return false;

    }

    //截取字符串
    public static String subString(String source, int startIndex, int length) {
        return source.substring(startIndex, startIndex + length);
    }

    public static String replaceLast(String text, String regex, String newStr) {
        return text.replaceFirst("(?s)" + regex + "(?!.*?" + regex + ")", newStr);
    }
}
