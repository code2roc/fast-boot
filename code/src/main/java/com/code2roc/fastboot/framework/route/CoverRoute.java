package com.code2roc.fastboot.framework.route;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CoverRoute {
    String value() default "";
}
