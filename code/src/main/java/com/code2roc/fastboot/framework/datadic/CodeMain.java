package com.code2roc.fastboot.framework.datadic;

import java.util.List;

public class CodeMain {
    private String codeName;
    private String codePackageName;
    private String codeClassName;
    private List<CodeItem> codeItemList;

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCodePackageName() {
        return codePackageName;
    }

    public void setCodePackageName(String codePackageName) {
        this.codePackageName = codePackageName;
    }

    public String getCodeClassName() {
        return codeClassName;
    }

    public void setCodeClassName(String codeClassName) {
        this.codeClassName = codeClassName;
    }

    public List<CodeItem> getCodeItemList() {
        return codeItemList;
    }

    public void setCodeItemList(List<CodeItem> codeItemList) {
        this.codeItemList = codeItemList;
    }
}
