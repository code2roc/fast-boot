package com.code2roc.fastboot.framework.redis;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;

import java.time.Duration;

public class PoolEntity extends RedisProperties.Pool {
    public PoolEntity(){
        setMaxActive(1000);
        setMaxIdle(8);
        setMaxWait(Duration.ofMillis(-1));
        setMaxIdle(0);
    }
}
