package com.code2roc.fastboot.framework.datawrapper;

import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.template.BaseSystemObject;
import com.code2roc.fastboot.framework.util.BeanUtil;
import com.code2roc.fastboot.framework.util.ConvertOp;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

@Aspect
@Component
@Order(value = 99)
public class WrapperAspect {

    @Pointcut("@annotation(com.code2roc.fastboot.framework.datawrapper.Wrapper) " +
            "|| @within(com.code2roc.fastboot.framework.datawrapper.Wrapper)"+
            "|| @annotation(com.code2roc.fastboot.framework.datawrapper.Sensitive)"+
            "|| @within(com.code2roc.fastboot.framework.datawrapper.Sensitive)")
    public void wrapperAspect() {

    }

    @Around("wrapperAspect()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object proceedResult = point.proceed();
        return processWrapping(point, proceedResult);
    }

    private Object processWrapping(ProceedingJoinPoint proceedingJoinPoint, Object originResult) throws Exception{
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();

        ///region 脱敏处理
        Sensitive sensitiveAnnotation = method.getAnnotation(Sensitive.class);
        if(sensitiveAnnotation!=null && originResult instanceof Result){
            Result result = (Result) originResult;
            Map<String, Object> data = result.getData();
            for (String key : data.keySet()) {
                Object value = data.get(key);
                if (value instanceof Collection) {
                    // 获取原有的List
                    Collection collection = (Collection) value;
                    // 将page中所有records都包装一遍
                    ArrayList<Map<String, Object>> maps = new ArrayList<>();
                    for (Object item : collection) {
                        if (item instanceof BaseSystemObject) {
                            sensitivePureObject(item);
                        }
                    }
                } else if (value instanceof BaseSystemObject) {
                    sensitivePureObject(value);
                }
            }
        }
        //endregion

        ///region wrapper处理
        Wrapper wrapperAnnotation = method.getAnnotation(Wrapper.class);
        if(wrapperAnnotation!=null){
            // 获取注解上的处理类
            Class<? extends BaseWrapper<?>>[] baseWrapperClasses = wrapperAnnotation.value();
            // 如果注解上的为空直接返回
            if (baseWrapperClasses == null || (baseWrapperClasses != null && baseWrapperClasses.length == 0)) {
                return originResult;
            }
            // 获取原有返回结果，如果不是ResponseData则不进行处理(需要遵守这个约定)
            if (!(originResult instanceof Result)) {
                return originResult;
            }
            Result result = (Result) originResult;
            Map<String, Object> data = result.getData();
            for (String key : data.keySet()) {
                Object value = data.get(key);
                if (value instanceof Collection) {
                    // 获取原有的List
                    Collection collection = (Collection) value;
                    // 将page中所有records都包装一遍
                    ArrayList<Map<String, Object>> maps = new ArrayList<>();
                    for (Object wrappedItem : collection) {
                        if (wrappedItem instanceof BaseSystemObject) {
                            maps.add(this.wrapPureObject(wrappedItem, baseWrapperClasses));
                        }
                    }
                    data.put(key, maps);
                } else if (value instanceof BaseSystemObject) {
                    Map<String, Object> map = this.wrapPureObject(value, baseWrapperClasses);
                    data.put(key, map);
                }

            }
        }

        ///endregion

        return originResult;
    }

    private Map<String, Object> wrapPureObject(Object originModel, Class<? extends BaseWrapper<?>>[] baseWrapperClasses) throws Exception {
        // 首先将原始的对象转化为map
        Map<String, Object> originMap = ConvertOp.convertBeanToMap(originModel);
        // 经过多个包装类填充属性
        for (Class<? extends BaseWrapper<?>> baseWrapperClass : baseWrapperClasses) {
            BaseWrapper baseWrapper = BeanUtil.getBean(baseWrapperClass);
            Map<String, Object> incrementFieldsMap = baseWrapper.doWrap(originModel);
            originMap.putAll(incrementFieldsMap);
        }
        return originMap;
    }

    private void sensitivePureObject(Object originModel) throws Exception {
        Class clazz = originModel.getClass();
        List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
        for (Field field:fs) {
            field.setAccessible(true);
            SensitiveField sensitiveField = field.getAnnotation(SensitiveField.class);
            if(sensitiveField!=null && sensitiveField.value()!=null){
                BaseSensitive sensitive = BeanUtil.getBean(sensitiveField.value());
                String newValue = sensitive.doSensitive(ConvertOp.convert2String(field.get(originModel)));
                field.set(originModel,newValue);
            }
        }
    }

}
