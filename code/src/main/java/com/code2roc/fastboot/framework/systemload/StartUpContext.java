package com.code2roc.fastboot.framework.systemload;

import com.code2roc.fastboot.framework.cache.CacheUtil;
import com.code2roc.fastboot.framework.database.MapperBeanManage;
import com.code2roc.fastboot.framework.datadic.DataDicManage;
import com.code2roc.fastboot.framework.global.SystemConfig;
import com.code2roc.fastboot.framework.route.ConverRouteUtil;
import com.code2roc.fastboot.framework.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.MultipartConfigElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Order(value = 10)
public class StartUpContext  implements ApplicationRunner {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private MapperBeanManage mapperBeanManage;
    @Autowired
    private DataDicManage dataDicManage;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private SystemConfig systemConfig;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        Date start = new Date();
        System.out.println("fastboot开始启动");

        SystemLoaderUtil.clearLoaderTag();

        Map<String, Object> annotatedBeans = context.getBeansWithAnnotation(SpringBootApplication.class);
        Class bootClass = annotatedBeans.values().toArray()[0].getClass();

        //清除静态文件
        ClassPathFileUtil.clearClassPathFileTemplateFolder();
        //扫描数据字典
        dataDicManage.initCodeItem(bootClass);
        //扫描dao
        mapperBeanManage.initMapperBean(bootClass);
        //初始化缓存
        cacheUtil.initCache(bootClass);
        //扫描路由覆盖
        ConverRouteUtil.initRoute(bootClass);
        //jsonfield个性序列化时间格式
        FastJsonUtil.scanDate2Json(bootClass);

        //执行自定义系统启动事件
        Map<String, ISystemStartExecute> res = context.getBeansOfType(ISystemStartExecute.class);
        for (Map.Entry en :res.entrySet()) {
            ISystemStartExecute service = (ISystemStartExecute)en.getValue();
            service.execute();
        }

        SystemLoaderUtil.updateFrameVersion();
        SystemLoaderUtil.addLoaderTag();

        long loadPeroid = DateUtil.getTimePeroidToNowMillSecond(start);
        String version = SystemLoaderUtil.getFrameVersion();
        System.out.println("【框架版本号】：" + version);
        System.out.println("【fastboot启动耗时】：" + loadPeroid + "毫秒");
    }
}
