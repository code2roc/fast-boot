package com.code2roc.fastboot.framework.template;

import com.code2roc.fastboot.framework.model.Page;

import java.util.HashMap;
import java.util.List;

public interface BaseService<T extends BaseModel> {
    void insert(T entity);

    void delete(String unitguid);

    void batchDelte(List<String> unitguidList);

    void update(T entity);

    void batchInsert(List<T> entityList);

    void batchUpdate(List<T> entityList);

    void save(T entity);

    T selectOne(String unitguid);

    Page selectPage(String columns, String where, String orderBy, HashMap<String, Object> paramMap);

    List<T> selectPageList(String columns, String where, String orderBy, HashMap<String, Object> paramMap);

    List<T> selectList(String columns, String where, String orderBy, HashMap<String, Object> paramMap);

    int selectCount(String where, HashMap<String, Object> paramMap);
}
