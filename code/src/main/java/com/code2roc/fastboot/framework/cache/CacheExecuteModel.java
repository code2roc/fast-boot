package com.code2roc.fastboot.framework.cache;

import com.code2roc.fastboot.framework.template.BaseSystemObject;

public class CacheExecuteModel {
    private String obejctClazzName;
    private String cacheName;
    private String key;
    private BaseSystemObject value;
    private String executeType;

    public CacheExecuteModel() {

    }

    public CacheExecuteModel(String executeType, String cacheName, String key) {
        this.executeType = executeType;
        this.cacheName = cacheName;
        this.key = key;
    }

    public CacheExecuteModel(String executeType, String cacheName) {
        this.executeType = executeType;
        this.cacheName = cacheName;
    }

    public CacheExecuteModel(String executeType, String cacheName, String key, String obejctClazzName, BaseSystemObject value) {
        this.executeType = executeType;
        this.cacheName = cacheName;
        this.key = key;
        this.obejctClazzName = obejctClazzName;
        this.value = value;
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public BaseSystemObject getValue() {
        return value;
    }

    public void setValue(BaseSystemObject value) {
        this.value = value;
    }

    public String getExecuteType() {
        return executeType;
    }

    public void setExecuteType(String executeType) {
        this.executeType = executeType;
    }

    public String getObejctClazzName() {
        return obejctClazzName;
    }

    public void setObejctClazzName(String obejctClazzName) {
        this.obejctClazzName = obejctClazzName;
    }
}
