package com.code2roc.fastboot.framework.batchop;

import java.util.List;

public class BatchTableInfo {
    private String sqlText;
    private List<BatchColumnInfo> paramList;
    private Object keyValue;

    public String getSqlText() {
        return sqlText;
    }

    public void setSqlText(String sqlText) {
        this.sqlText = sqlText;
    }

    public List<BatchColumnInfo> getParamList() {
        return paramList;
    }

    public void setParamList(List<BatchColumnInfo> paramList) {
        this.paramList = paramList;
    }

    public Object getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(Object keyValue) {
        this.keyValue = keyValue;
    }
}
