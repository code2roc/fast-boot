package com.code2roc.fastboot.system.setting.service;

import com.code2roc.fastboot.system.setting.model.SystemCodeMainDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemCodeMainService  extends BaseBootService<SystemCodeMainDO> {
}
