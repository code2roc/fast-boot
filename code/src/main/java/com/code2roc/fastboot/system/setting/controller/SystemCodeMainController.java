package com.code2roc.fastboot.system.setting.controller;

import com.code2roc.fastboot.framework.cache.AutoRefreshCache;
import com.code2roc.fastboot.system.enums.CommonEnum;
import com.code2roc.fastboot.system.setting.bizlogic.DataDicLogic;
import com.code2roc.fastboot.system.setting.model.SystemCodeMainDO;
import com.code2roc.fastboot.system.setting.service.ISystemCodeMainService;
import com.code2roc.fastboot.system.setting.service.ISystemGroupService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/setting/codemain")
public class SystemCodeMainController extends BaseBootController {
    @Autowired
    private ISystemCodeMainService service;
    @Autowired
    private ISystemGroupService groupService;
    @Autowired
    private DataDicLogic dataDicLogic;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String code_name = ConvertOp.convert2String(params.get("code_name"));
        String group_id = ConvertOp.convert2String(params.get("group_id"));
        if (StringUtil.isEmpty(group_id)) {
            if (!StringUtil.isEmpty(code_name)) {
                sql += " and code_name like #{code_name}";
                paramMap.put("code_name", "%" + code_name + "%");
            }
        } else {
            sql += groupService.getGroupIDSearchSQL(group_id);
            if (!StringUtil.isEmpty(code_name)) {
                sql += " and code_name like #{code_name}";
                paramMap.put("code_name", "%" + code_name + "%");
            }
        }

        List<SystemCodeMainDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/insert")
    @AutoRefreshCache("DataBaseDic")
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object insert(@RequestBody SystemCodeMainDO entity) {
        Result result = Result.okResult();
        if (service.checkExist("code_name", entity.getCode_name())) {
            return Result.errorResult().setMsg("数据字典已存在");
        }
        dataDicLogic.insert(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/detail")
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        result.add("obj", dataDicLogic.detail(row_id));
        return result;
    }

    @ResponseBody
    @PostMapping("/update")
    @AutoRefreshCache("DataBaseDic")
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object update(@UpdateRequestBody SystemCodeMainDO entity) {
        Result result = Result.okResult();
        if (service.checkExist("code_name", entity.getCode_name(), entity.getRow_id())) {
            return Result.errorResult().setMsg("数据字典已存在");
        }
        dataDicLogic.update(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/delete")
    @AutoRefreshCache("DataBaseDic")
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object delete(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        SystemCodeMainDO entity = service.selectOne(row_id);
        if (entity.getSystem_init() == CommonEnum.YesOrNo.Yes.get_value()) {
            return Result.errorResult().setMsg("系统内置数据，禁止删除");
        }
        dataDicLogic.delete(row_id);
        return result;
    }

    @ResponseBody
    @PostMapping("/exportData")
    public Object exportData(@RequestBody List<String> codeIDList) throws Exception {
        Result result = Result.okResult();
        String fileid = dataDicLogic.exportData(codeIDList);
        result.add("fileid", fileid);
        return result;
    }

    @ResponseBody
    @PostMapping("/importData")
    @AutoRefreshCache("DataBaseDic")
    public Object importData(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        Result result = Result.okResult();
        String errormsg = dataDicLogic.importData(file.getInputStream());
        if (!StringUtil.isEmpty(errormsg)) {
            return Result.errorResult().setMsg(errormsg);
        }
        return result;
    }
}
