package com.code2roc.fastboot.system.codegen.service;

import com.code2roc.fastboot.system.codegen.model.SystemTableDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemTableService extends BaseBootService<SystemTableDO> {
}
