package com.code2roc.fastboot.system.cache;

import com.code2roc.fastboot.framework.cache.CacheUtil;
import com.code2roc.fastboot.framework.cache.ICacheInitHandler;
import com.code2roc.fastboot.framework.datadic.CodeItem;
import com.code2roc.fastboot.system.setting.model.SystemCodeItemDO;
import com.code2roc.fastboot.system.setting.model.SystemCodeMainDO;
import com.code2roc.fastboot.system.setting.service.ISystemCodeItemService;
import com.code2roc.fastboot.system.setting.service.ISystemCodeMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataBaseDicCacheService implements ICacheInitHandler {
    @Autowired
    private ISystemCodeMainService codeMainService;
    @Autowired
    private ISystemCodeItemService codeItemService;
    @Autowired
    private CacheUtil cacheUtil;

    @Override
    public String getCacheName() {
        return "DataBaseDic";
    }

    @Override
    public void initCache() {
        List<SystemCodeMainDO> codeMainDOList = codeMainService.selectList("*", "1=1", "", null);
        List<SystemCodeItemDO> codeItemDOList = codeItemService.selectList("*", "1=1", "sort_num desc", null);
        List<SystemCodeItemDO> codeItemFilterList = new ArrayList<>();
        for (SystemCodeMainDO entity : codeMainDOList) {
            codeItemFilterList = codeItemDOList.stream().filter(a -> a.getCode_id().equals(entity.getRow_id())).collect(Collectors.toList());
            List<CodeItem> codeItemList = new ArrayList<>();
            for (SystemCodeItemDO itemDO : codeItemFilterList) {
                CodeItem codeItem = new CodeItem();
                codeItem.set_itemText(itemDO.getItem_text());
                codeItem.set_itemValue(itemDO.getItem_value());
                codeItemList.add(codeItem);
            }
            DataBaseDicCacheInfo dataBaseDicCacheInfo = new DataBaseDicCacheInfo();
            dataBaseDicCacheInfo.setCodeName(entity.getCode_name());
            dataBaseDicCacheInfo.setCodeItemList(codeItemList);
            cacheUtil.putCache(getCacheName(), entity.getCode_name(), dataBaseDicCacheInfo);
        }
    }

    @Override
    public void initKeyCache(String key) {

    }

    @Override
    public String getHeap() {
        return "1000";
    }

    @Override
    public String getOffheap() {
        return "20";
    }

    @Override
    public String getDisk() {
        return "100";
    }
}
