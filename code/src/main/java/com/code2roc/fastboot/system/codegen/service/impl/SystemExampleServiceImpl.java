package com.code2roc.fastboot.system.codegen.service.impl;

import com.code2roc.fastboot.system.codegen.model.SystemExampleDO;
import com.code2roc.fastboot.system.codegen.service.ISystemExampleService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SystemExampleServiceImpl extends BaseBootServiceImpl<SystemExampleDO> implements ISystemExampleService {

}