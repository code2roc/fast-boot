package com.code2roc.fastboot.system.setting.service.impl;

import com.code2roc.fastboot.system.setting.model.SystemMenuSettingDO;
import com.code2roc.fastboot.system.setting.service.ISystemMenuSettingService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class SystemMenuSettingServiceImpl extends BaseBootServiceImpl<SystemMenuSettingDO> implements ISystemMenuSettingService {
    @Override
    public void deleteSetting(String menu_id, String setting_id) {
        String sql = "delete from system_menu_setting where menu_id = #{menu_id} and setting_id = #{setting_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("menu_id",menu_id);
        paramMap.put("setting_id",setting_id);
        commonDTO.executeSQL(sql,paramMap);
    }

    @Override
    public void deleteSetting(String menu_id) {
        String sql = "delete from system_menu_setting where menu_id = #{menu_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("menu_id",menu_id);
        commonDTO.executeSQL(sql,paramMap);
    }

    @Override
    public void deleteSettingBySettingID(String setting_id) {
        String sql = "delete from system_menu_setting where setting_id = #{setting_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("setting_id",setting_id);
        commonDTO.executeSQL(sql,paramMap);
    }
}
