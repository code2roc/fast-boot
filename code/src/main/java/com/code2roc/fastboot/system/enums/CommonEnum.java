package com.code2roc.fastboot.system.enums;

import com.code2roc.fastboot.framework.datadic.IConvertEnumToCodeItem;

public class CommonEnum {
    public enum YesOrNo implements IConvertEnumToCodeItem {
        Yes(10, "是"), No(20, "否");
        private int _value;
        private String _name;

        private YesOrNo(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "是否";
        }
    }

    public enum EnableOrDisable implements IConvertEnumToCodeItem {
        Enable(10, "启用"), Disable(20, "禁用");
        private int _value;
        private String _name;

        private EnableOrDisable(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "启用禁用";
        }
    }
}
