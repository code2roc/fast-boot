package com.code2roc.fastboot.system.setting.service.impl;

import com.code2roc.fastboot.system.setting.model.SystemCodeMainDO;
import com.code2roc.fastboot.system.setting.service.ISystemCodeMainService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SystemCodeMainServiceImpl extends BaseBootServiceImpl<SystemCodeMainDO> implements ISystemCodeMainService {
}
