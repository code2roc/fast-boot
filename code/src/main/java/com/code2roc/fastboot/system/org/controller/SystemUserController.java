package com.code2roc.fastboot.system.org.controller;

import com.code2roc.fastboot.model.ZTreeNode;
import com.code2roc.fastboot.system.org.bizlogic.UserLogic;
import com.code2roc.fastboot.system.org.model.SystemDeptDO;
import com.code2roc.fastboot.system.org.model.SystemUserDO;
import com.code2roc.fastboot.system.org.service.ISystemDeptService;
import com.code2roc.fastboot.system.org.service.ISystemUserService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.util.ZTreeBuilder;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.EncryptUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/org/user")
public class SystemUserController extends BaseBootController {
    @Autowired
    private ISystemDeptService deptService;
    @Autowired
    private ISystemUserService service;
    @Autowired
    private UserLogic userLogic;

    @ResponseBody
    @PostMapping("/tree")
    public Object tree(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        List<SystemDeptDO> entityList =  deptService.selectAllList();
        List<SystemUserDO> userDOList =  service.selectAllList();
        List<ZTreeNode> treeNodeList = new ArrayList<ZTreeNode>();
        ZTreeNode rootNode = new ZTreeNode();
        rootNode.setId("Top");
        rootNode.setpId("-1");
        rootNode.setName("根");
        rootNode.setOpen(true);
        rootNode.setTopNode(true);
        rootNode.setNocheck(true);
        treeNodeList.add(rootNode);
        for (SystemDeptDO entity:entityList) {
            ZTreeNode node = new ZTreeNode();
            node.setId(entity.getRow_id());
            node.setpId(entity.getParent_id());
            node.setName(entity.getDept_name());
            node.setSotNum(entity.getSort_num());
            node.setNocheck(true);
            treeNodeList.add(node);
        }
        for (SystemUserDO entity:userDOList) {
            ZTreeNode node = new ZTreeNode();
            node.setId(entity.getRow_id());
            node.setpId(entity.getDept_id());
            node.setName(entity.getUser_name());
            node.setSotNum(entity.getSort_num());
            treeNodeList.add(node);
        }
        List<ZTreeNode> obj = ZTreeBuilder.BuildTree(treeNodeList);
        return result.add("obj",obj);
    }

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String login_name = ConvertOp.convert2String(params.get("login_name"));
        String user_name = ConvertOp.convert2String(params.get("user_name"));
        String dept_id = ConvertOp.convert2String(params.get("dept_id"));
        if (dept_id.equals("Top")) {
            if (!StringUtil.isEmpty(login_name)) {
                sql += " and login_name like #{login_name}";
                paramMap.put("login_name", "%" + login_name + "%");
            }
            if (!StringUtil.isEmpty(user_name)) {
                sql += " and user_name like #{user_name}";
                paramMap.put("user_name", "%" + user_name + "%");
            }
        } else {
            String guidString = "";
            List<String> guidList = deptService.getAllSubDeptIDList(dept_id).stream().distinct().collect(Collectors.toList());
            if (guidList.size() == 0) {
                guidString = "''";
            } else {
                for (int i = 0; i < guidList.size(); i++) {
                    if (i == guidList.size() - 1) {
                        guidString += "'" + guidList.get(i) + "'";
                    } else {
                        guidString += "'" + guidList.get(i) + "',";
                    }
                }
            }
            sql += " and dept_id in (" + guidString + ")";
            if (!StringUtil.isEmpty(login_name)) {
                sql += " and login_name like #{login_name}";
                paramMap.put("login_name", "%" + login_name + "%");
            }
            if (!StringUtil.isEmpty(user_name)) {
                sql += " and user_name like #{user_name}";
                paramMap.put("user_name", "%" + user_name + "%");
            }
        }

        List<SystemDeptDO> deptDOList = deptService.selectAllList();
        List<SystemUserDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        for (SystemUserDO  entity:rows) {
            entity.setDeptFullPath(deptService.getFullPath(entity.getDept_id(),deptDOList));
        }
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/insert")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object insert(@RequestBody SystemUserDO entity) throws Exception {
        Result result = Result.okResult();
        if(service.checkExist("login_name",entity.getLogin_name())){
            return Result.errorResult().setMsg("登录名已存在");
        }
        userLogic.insert(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/detail")
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        result.add("obj", userLogic.detail(row_id));
        return result;
    }

    @ResponseBody
    @PostMapping("/update")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object update(@UpdateRequestBody SystemUserDO entity) {
        Result result = Result.okResult();
        userLogic.update(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/batchUpdate")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object batchUpdate(@UpdateRequestBody List<SystemUserDO> entityList) {
        Result result = Result.okResult();
        service.batchUpdate(entityList);
        return result;
    }

    @ResponseBody
    @PostMapping("/delete")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object delete(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        userLogic.delete(row_id);
        return result;
    }

    @ResponseBody
    @PostMapping("/initPassword")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object initPassword(@RequestBody Map<String, Object> params) throws Exception {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        userLogic.initPassword(row_id);
        return result;
    }

    @ResponseBody
    @PostMapping("/changePassword")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object changePassword(@RequestBody Map<String, Object> params) throws Exception {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        String oldpassword = ConvertOp.convert2String(params.get("oldpassword"));
        String newpassword = ConvertOp.convert2String(params.get("newpassword"));
        SystemUserDO dbuser = service.selectOne(authUtil.getUserTokenModel().getUserID());
        String oldmd5 = EncryptUtil.encryptByMD5(dbuser.getLogin_name() + "|" + oldpassword);
        String newmd5 = EncryptUtil.encryptByMD5(dbuser.getLogin_name() + "|" + newpassword);
        if(dbuser.getPassword().equals(oldmd5)){
            if(oldmd5.equals(newmd5)){
                return Result.errorResult().setMsg("修改密码相同");
            }else{
                userLogic.changePassword(authUtil.getUserTokenModel().getUserID(),newmd5);
            }
        }else{
            return Result.errorResult().setMsg("密码错误");
        }
        return result;
    }

    @ResponseBody
    @PostMapping("/changeStatus")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object changeStatus(@RequestBody Map<String, Object> params) throws Exception {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        userLogic.changeStatus(row_id);
        return result;
    }

    @ResponseBody
    @PostMapping("/exportData")
    public Object exportData(@RequestBody List<String> userIDList) throws Exception {
        Result result = Result.okResult();
        String fileid = userLogic.exportData(userIDList);
        result.add("fileid", fileid);
        return result;
    }

    @ResponseBody
    @PostMapping("/importData")
    public Object importData(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        Result result = Result.okResult();
        String errormsg = userLogic.importData(file.getInputStream());
        if (!StringUtil.isEmpty(errormsg)) {
            return Result.errorResult().setMsg(errormsg);
        }
        return result;
    }

}
