package com.code2roc.fastboot.system.setting.service.impl;

import com.code2roc.fastboot.system.enums.MenuEnum;
import com.code2roc.fastboot.system.setting.model.SystemMenuDO;
import com.code2roc.fastboot.system.setting.service.ISystemMenuService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class SystemMenuServiceImpl extends BaseBootServiceImpl<SystemMenuDO> implements ISystemMenuService {
    @Override
    public void insert(SystemMenuDO entity) {
        entity.setPublic_status(MenuEnum.PublicStatus.NoPublic.get_value());
        super.insert(entity);
    }

    @Override
    public List<SystemMenuDO> selectSubMenuByMenuCode(String menu_code) {
        HashMap paramMap = new HashMap<>();
        String sql = " menu_code like #{menu_code}";
        paramMap.put("menu_code",menu_code + "%");
        return selectList("*",sql,"sort_num desc",paramMap);
    }
}
