package com.code2roc.fastboot.system.setting.controller;

import com.alibaba.fastjson.JSON;
import com.code2roc.fastboot.system.enums.MenuEnum;
import com.code2roc.fastboot.system.org.model.SystemRoleDO;
import com.code2roc.fastboot.system.org.service.ISystemDeptService;
import com.code2roc.fastboot.system.org.service.ISystemRoleService;
import com.code2roc.fastboot.system.org.service.ISystemUserService;
import com.code2roc.fastboot.system.setting.bizlogic.MenuLogic;
import com.code2roc.fastboot.system.setting.model.SystemMenuDO;
import com.code2roc.fastboot.system.setting.model.SystemMenuSettingDO;
import com.code2roc.fastboot.system.setting.service.ISystemMenuService;
import com.code2roc.fastboot.system.setting.service.ISystemMenuSettingService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/setting/menusetting")
public class SystemMenuSettingController extends BaseBootController {
    @Autowired
    private ISystemMenuService service;
    @Autowired
    private ISystemRoleService roleService;
    @Autowired
    private ISystemDeptService deptService;
    @Autowired
    private ISystemUserService userService;
    @Autowired
    private ISystemMenuSettingService menuSettingService;
    @Autowired
    private MenuLogic menuLogic;

    @ResponseBody
    @PostMapping("/getMenuSettingList")
    public Object getMenuSettingList(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String menu_name = ConvertOp.convert2String(params.get("menu_name"));
        String menu_code = ConvertOp.convert2String(params.get("menu_code"));
        if (!StringUtil.isEmpty(menu_name)) {
            sql += " and menu_name like #{menu_name}";
            paramMap.put("menu_name", "%" + menu_name + "%");
        }
        if(!StringUtil.isEmpty(menu_code)){
            sql += " and menu_code like '" + menu_code + "%' and menu_code!='" + menu_code + "' and length(menu_code)=" + (menu_code.length() + 4) + "";
        }else{
            sql +=" and length(menu_code) =" + (menu_code.length() + 4);
        }
        List<SystemRoleDO> roleDOList = roleService.selectAllList();
        List<SystemMenuSettingDO> menuSettingDOList = menuSettingService.selectAllList();
        List<SystemMenuDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        for (SystemMenuDO entity:rows) {
            entity.setRoleList(menuLogic.getMenuRoleList(entity.getRow_id(),roleDOList,menuSettingDOList));
        }
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/updateSetting")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object updateSetting(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String menu_id = ConvertOp.convert2String(params.get("menu_id"));
        String role_id = ConvertOp.convert2String(params.get("role_id"));
        boolean regist = ConvertOp.convert2Boolean(params.get("regist"));
        menuLogic.updateRoleSetting(menu_id, role_id, regist);
        return result;
    }

    @ResponseBody
    @PostMapping("/getMenuSetting")
    public Object getMenuSetting(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String menu_id = ConvertOp.convert2String(params.get("menu_id"));
        List<SystemRoleDO> roleDOList = roleService.selectAllList();
        List<SystemMenuSettingDO> settingDOList = menuSettingService.selectListByField("menu_id",menu_id);
        //角色授权
        List<SystemRoleDO> roleSettingList = menuLogic.getMenuRoleList(menu_id,roleDOList,settingDOList);
        //部门授权
        List<String> deptSettingList = new ArrayList<>();
        deptSettingList = settingDOList.stream().filter(a->a.getSetting_mode()== MenuEnum.SettingModel.DeptSetting.get_value()).map(a->a.getSetting_id()).collect(Collectors.toList());
        List<String> deptSettingNameList = deptService.selectRangeList(deptSettingList).stream().map(a->a.getDept_name()).collect(Collectors.toList());
        //人员授权
        List<String> userSettingList = new ArrayList<>();
        userSettingList = settingDOList.stream().filter(a->a.getSetting_mode()== MenuEnum.SettingModel.UserSetting.get_value()).map(a->a.getSetting_id()).collect(Collectors.toList());
        List<String> userSettingNameList = userService.selectRangeList(userSettingList).stream().map(a->a.getUser_name()).collect(Collectors.toList());
        result.add("roleSettingList",roleSettingList);
        result.add("deptSettingList",deptSettingList);
        result.add("deptSettingNameList",deptSettingNameList);
        result.add("userSettingList",userSettingList);
        result.add("userSettingNameList",userSettingNameList);
        return result;
    }

    @ResponseBody
    @PostMapping("/updateMenuSetting")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object updateMenuSetting(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String menu_id = ConvertOp.convert2String(params.get("menu_id"));
        String deptSettingListStr =  ConvertOp.convert2String(params.get("deptSettingList"));
        String userSettingListStr =  ConvertOp.convert2String(params.get("userSettingList"));
        String roleSettingListStr =  ConvertOp.convert2String(params.get("roleSettingList"));
        List<String> deptSettingList =  JSON.parseArray(deptSettingListStr,String.class);
        List<String> userSettingList =  JSON.parseArray(userSettingListStr,String.class);
        List<String> roleSettingList = JSON.parseArray(roleSettingListStr,String.class);
        menuLogic.updateMenuSetting(menu_id,deptSettingList,userSettingList,roleSettingList);
        return result;
    }
}
