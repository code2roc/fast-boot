package com.code2roc.fastboot.system.common.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.code2roc.fastboot.framework.cache.CacheModel;
import com.code2roc.fastboot.framework.cache.CacheUtil;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import com.code2roc.fastboot.template.BaseBootController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/common/cache")
public class CacheController extends BaseBootController {
    @Autowired
    private CacheUtil cacheUtil;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        int pageIndex = ConvertOp.convert2Int(params.get("pageIndex"));
        int pageSize = ConvertOp.convert2Int(params.get("pageSize"));
        String cacheName = ConvertOp.convert2String(params.get("cacheName"));
        List<CacheModel> cacheList = cacheUtil.getCacheList();
        if(!StringUtil.isEmpty(cacheName)){
            cacheList = cacheList.stream().filter(a->a.getCacheName().contains(cacheName)).collect(Collectors.toList());
        }
        List<CacheModel> rows = new ArrayList<>();
        for (int i = (pageIndex - 1) * pageSize; i < pageIndex * pageSize && i < cacheList.size(); i++) {
            rows.add(cacheList.get(i));
        }
        result.add("rows", rows);
        result.add("total", cacheList.size());
        return result;
    }

    @ResponseBody
    @PostMapping("/refresh")
    public Object refresh(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String cacheName = ConvertOp.convert2String(params.get("cacheName"));
        cacheUtil.refreshCache(cacheName);
        return result;
    }

    @ResponseBody
    @PostMapping("/getCacheKeyList")
    public Object getCacheKeyList(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String cacheName = ConvertOp.convert2String(params.get("cacheName"));
        String cacheKey = ConvertOp.convert2String(params.get("cacheKey"));
        int pageIndex = ConvertOp.convert2Int(params.get("pageIndex"));
        int pageSize = ConvertOp.convert2Int(params.get("pageSize"));
        List<String> cacheKeyList = cacheUtil.getAllCacheKey(cacheName);
        if(!StringUtil.isEmpty(cacheName)){
            cacheKeyList = cacheKeyList.stream().filter(a->a.contains(cacheKey)).collect(Collectors.toList());
        }
        JSONArray rows = new JSONArray();
        for (int i = (pageIndex - 1) * pageSize; i < pageIndex * pageSize && i < cacheKeyList.size(); i++) {
            JSONObject obj = new JSONObject();
            obj.put("key",cacheKeyList.get(i));
            rows.add(obj);
        }
        result.add("rows", rows);
        result.add("total", cacheKeyList.size());
        return result;
    }

    @ResponseBody
    @PostMapping("/getCacheDetail")
    public Object getCacheDetail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String cacheName = ConvertOp.convert2String(params.get("cacheName"));
        String key = ConvertOp.convert2String(params.get("key"));
        result.add("obj", cacheUtil.getCache(cacheName, key));
        return result;
    }
}
