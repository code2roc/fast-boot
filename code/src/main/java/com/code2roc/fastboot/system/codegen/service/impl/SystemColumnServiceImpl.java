package com.code2roc.fastboot.system.codegen.service.impl;

import com.code2roc.fastboot.system.codegen.model.SystemColumnDO;
import com.code2roc.fastboot.system.codegen.service.ISystemColumnService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SystemColumnServiceImpl extends BaseBootServiceImpl<SystemColumnDO> implements ISystemColumnService {
}
