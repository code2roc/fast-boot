package com.code2roc.fastboot.system.org.service.impl;

import com.code2roc.fastboot.system.org.model.SystemRoleDO;
import com.code2roc.fastboot.system.org.service.ISystemRoleService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SystemRoleServiceImpl extends BaseBootServiceImpl<SystemRoleDO> implements ISystemRoleService {
}
