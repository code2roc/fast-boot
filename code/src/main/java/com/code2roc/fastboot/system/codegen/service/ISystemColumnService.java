package com.code2roc.fastboot.system.codegen.service;

import com.code2roc.fastboot.system.codegen.model.SystemColumnDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemColumnService extends BaseBootService<SystemColumnDO> {
}
