package com.code2roc.fastboot.system.codegen.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;
import java.math.BigDecimal;

@TableName("system_example")
public class SystemExampleDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;

    @TableField(value = "sort_num")
    private Integer sort_num;

    @TableField(value = "gmt_create")
    private Date gmt_create;

    @TableField(value = "gmt_modified")
    private Date gmt_modified;

    @TableField(value = "account_name")
    private String account_name;

    @TableField(value = "account_no")
    private String account_no;

    @TableField(value = "account_limit")
    private BigDecimal account_limit;

    @TableField(value = "open_date")
    @JSONField(format = "yyyy-MM-dd")
    private Date open_date;

    @TableField(value = "account_type")
    private Integer account_type;

    @TableField(value = "account_rules")
    private String account_rules;

    @TableField(value = "account_status")
    private Integer account_status;

    @TableField(value = "account_roles")
    private String account_roles;

    @TableField(value = "account_user_id")
    private String account_user_id;

    @TableField(value = "account_user_name")
    private String account_user_name;

    @TableField(value = "manage_userid_list")
    private String manage_userid_list;

    @TableField(value = "manage_username_list")
    private String manage_username_list;

    @TableField(value = "account_vip")
    private Integer account_vip;

    @Override
    public String getRow_id() {
        return row_id;
    }

    @Override
    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    @Override
    public Integer getSort_num() {
        return sort_num;
    }

    @Override
    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    @Override
    public Date getGmt_create() {
        return gmt_create;
    }

    @Override
    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    @Override
    public Date getGmt_modified() {
        return gmt_modified;
    }

    @Override
    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public BigDecimal getAccount_limit() {
        return account_limit;
    }

    public void setAccount_limit(BigDecimal account_limit) {
        this.account_limit = account_limit;
    }

    public Date getOpen_date() {
        return open_date;
    }

    public void setOpen_date(Date open_date) {
        this.open_date = open_date;
    }

    public Integer getAccount_type() {
        return account_type;
    }

    public void setAccount_type(Integer account_type) {
        this.account_type = account_type;
    }

    public String getAccount_rules() {
        return account_rules;
    }

    public void setAccount_rules(String account_rules) {
        this.account_rules = account_rules;
    }

    public Integer getAccount_status() {
        return account_status;
    }

    public void setAccount_status(Integer account_status) {
        this.account_status = account_status;
    }

    public String getAccount_roles() {
        return account_roles;
    }

    public void setAccount_roles(String account_roles) {
        this.account_roles = account_roles;
    }

    public String getAccount_user_id() {
        return account_user_id;
    }

    public void setAccount_user_id(String account_user_id) {
        this.account_user_id = account_user_id;
    }

    public String getAccount_user_name() {
        return account_user_name;
    }

    public void setAccount_user_name(String account_user_name) {
        this.account_user_name = account_user_name;
    }

    public String getManage_userid_list() {
        return manage_userid_list;
    }

    public void setManage_userid_list(String manage_userid_list) {
        this.manage_userid_list = manage_userid_list;
    }

    public String getManage_username_list() {
        return manage_username_list;
    }

    public void setManage_username_list(String manage_username_list) {
        this.manage_username_list = manage_username_list;
    }

    public Integer getAccount_vip() {
        return account_vip;
    }

    public void setAccount_vip(Integer account_vip) {
        this.account_vip = account_vip;
    }

}