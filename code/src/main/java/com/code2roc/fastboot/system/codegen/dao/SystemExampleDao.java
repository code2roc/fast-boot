package com.code2roc.fastboot.system.codegen.dao;

import com.code2roc.fastboot.system.codegen.model.SystemExampleDO;;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemExampleDao extends BaseMapper<SystemExampleDO> {

}