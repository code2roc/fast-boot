package com.code2roc.fastboot.system.setting.service;

import com.code2roc.fastboot.system.setting.model.SystemMenuDO;
import com.code2roc.fastboot.template.BaseBootService;

import java.util.List;

public interface ISystemMenuService extends BaseBootService<SystemMenuDO> {
    List<SystemMenuDO> selectSubMenuByMenuCode(String menu_code);
}
