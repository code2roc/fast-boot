package com.code2roc.fastboot.system.codegen.service;


import com.code2roc.fastboot.system.codegen.model.SystemExampleDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemExampleService extends BaseBootService<SystemExampleDO> {

}