package com.code2roc.fastboot.system.common.bizlogic;

import com.code2roc.fastboot.framework.file.FileStoargeProperty;
import com.code2roc.fastboot.system.common.model.SystemAttachDO;
import com.code2roc.fastboot.system.common.service.ISystemAttachService;
import com.code2roc.fastboot.template.BaseBootLogic;
import com.code2roc.fastboot.util.AuthUtil;
import com.code2roc.fastboot.framework.file.IFileOperate;
import com.code2roc.fastboot.framework.file.LocalFileOperate;
import com.code2roc.fastboot.framework.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;

@Component
public class AttachLogic extends BaseBootLogic {
    @Autowired
    private IFileOperate fileOperate;
    @Autowired
    private ISystemAttachService attachService;
    @Autowired
    private AuthUtil authUtil;
    private String cloudBucketName = "fastboot";

    public void uploadAttach(String attach_name, long attach_size, String group_guid, String group_type, InputStream fileContent) throws Exception {
        SystemAttachDO attachDO = new SystemAttachDO();
        String attachGuid = CommonUtil.getNewGuid();
        attachDO.setRow_id(attachGuid);
        attachDO.setAttach_name(attach_name);
        String fileType = attach_name.substring(attach_name.lastIndexOf(".") + 1);
        attachDO.setAttach_type(fileType);
        attachDO.setAttach_size(attach_size);
        attachDO.setAttach_virtual_path(group_type + "/" + attachGuid + "/" + attach_name);
        attachDO.setGroup_guid(group_guid);
        attachDO.setGroup_type(group_type);
        attachDO.setAttach_status(10);
        attachDO.setUpload_user_id(authUtil.getUserTokenModel().getUserID());
        attachDO.setUpload_user_name(authUtil.getUserTokenModel().getUserName());
        attachService.insert(attachDO);

        if (checkLocalStorage()) {
            fileOperate.storageFile(group_type + "/" + attachGuid, attach_name, fileContent);
        } else {
            fileOperate.storageFile(cloudBucketName, group_type + "/" + attachGuid + "/" + attach_name, fileContent);
        }

    }

    public void uploadChunkAttach(String attachGuid, String attach_name, long attach_size, String group_guid, String group_type, String fileMD5, int chunkCount, int chunkIndex, int chunkSize, InputStream fileContent) throws Exception {

        if (checkLocalStorage()) {
            fileOperate.storageFileByChunk(group_type + "/" + attachGuid, attach_name, fileContent, fileMD5, chunkCount, chunkIndex, chunkSize);
        } else {
            fileOperate.storageFileByChunk(cloudBucketName, group_type + "/" + attachGuid + "/" + attach_name, fileContent, fileMD5, chunkCount, chunkIndex, chunkSize);
        }
        if (chunkIndex == chunkCount - 1) {
            SystemAttachDO attachDO = new SystemAttachDO();
            attachDO.setRow_id(attachGuid);
            attachDO.setAttach_name(attach_name);
            String fileType = attach_name.substring(attach_name.lastIndexOf(".") + 1);
            attachDO.setAttach_type(fileType);
            attachDO.setAttach_size(attach_size);
            attachDO.setAttach_virtual_path(group_type + "/" + attachGuid + "/" + attach_name);
            attachDO.setGroup_guid(group_guid);
            attachDO.setGroup_type(group_type);
            attachDO.setAttach_status(10);
            attachDO.setUpload_user_id(authUtil.getUserTokenModel().getUserID());
            attachDO.setUpload_user_name(authUtil.getUserTokenModel().getUserName());
            attachService.insert(attachDO);
        }
    }

    public String getAttachUrl(String row_id) throws Exception {
        SystemAttachDO attachDO = attachService.selectOne(row_id);
        String bucket = attachDO.getGroup_type() + "/" + row_id;
        String fileUrl = "";
        if (checkLocalStorage()) {
            fileUrl = CommonUtil.getApiPath() + "/system/common/attach/downLocal?row_id=" + row_id;
        } else {
            fileUrl = fileOperate.getFileUrl(cloudBucketName, bucket + "/" + attachDO.getAttach_name());
        }
        return fileUrl;
    }

    public void deleteAttach(String row_id) throws Exception {
        SystemAttachDO attachDO = attachService.selectOne(row_id);
        attachService.delete(row_id);
        String bucket = attachDO.getGroup_type() + "/" + row_id;
        if (checkLocalStorage()) {
            fileOperate.deleteFile(bucket, attachDO.getAttach_name());
        } else {
            fileOperate.deleteFile(cloudBucketName, bucket + "/" + attachDO.getAttach_name());
        }

    }

    public byte[] getAttachBytes(String row_id) throws Exception {
        SystemAttachDO attachDO = attachService.selectOne(row_id);
        String bucket = attachDO.getGroup_type() + "/" + row_id;
        if (checkLocalStorage()) {
            return fileOperate.getFileBytes(bucket, attachDO.getAttach_name());
        } else {
            return fileOperate.getFileBytes(cloudBucketName, bucket + "/" + attachDO.getAttach_name());
        }

    }

    public void copyAttach(String row_id, String new_group_guid) throws Exception {
        SystemAttachDO attachDO = attachService.selectOne(row_id);
        String oldAttachGuid = attachDO.getRow_id();
        String newAttachGuid = CommonUtil.getNewGuid();
        attachDO.setRow_id(newAttachGuid);
        attachDO.setGroup_guid(new_group_guid);
        attachDO.setAttach_virtual_path(attachDO.getGroup_type() + "/" + newAttachGuid + "/" + attachDO.getAttach_name());
        attachService.insert(attachDO);
        String oldBucket = attachDO.getGroup_type() + "/" + oldAttachGuid;
        String newBucket = attachDO.getGroup_type() + "/" + newAttachGuid;
        if (checkLocalStorage()) {
            fileOperate.copyFile(oldBucket, attachDO.getAttach_name(), newBucket, attachDO.getAttach_name());
        } else {
            fileOperate.copyFile(cloudBucketName, oldBucket + "/" + attachDO.getAttach_name(), cloudBucketName, newBucket + "/" + attachDO.getAttach_name());
        }
    }

    public void copyGroupAttach(String group_guid, String new_group_guid) throws Exception {
        List<SystemAttachDO> attachDOList = attachService.selectListByGroupGuid(group_guid);
        for (SystemAttachDO attachDO : attachDOList) {
            copyAttach(attachDO.getRow_id(), new_group_guid);
        }
    }

    public void copyGroupAttach(String group_guid, String group_type, String new_group_guid) throws Exception {
        List<SystemAttachDO> attachDOList = attachService.selectListByGroupGuid(group_guid, group_type);
        for (SystemAttachDO attachDO : attachDOList) {
            copyAttach(attachDO.getRow_id(), new_group_guid);
        }
    }

    public boolean checkLocalStorage() {
        if (fileOperate.getClass() == LocalFileOperate.class) {
            return true;
        } else {
            return false;
        }
    }

    public FileStoargeProperty getFileStoargeProperty(){
        return fileOperate.getFileStoargeProperty();
    }
}
