package com.code2roc.fastboot.system.codegen.enums;

import com.code2roc.fastboot.framework.datadic.IConvertEnumToCodeItem;

public class ExampleEnum {
    public enum AccountType implements IConvertEnumToCodeItem {
        CommonAccount(10, "普通账户"), SpecialAccount(20, "特殊账户");
        private int _value;
        private String _name;

        private AccountType(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "账户类型";
        }
    }
}
