package com.code2roc.fastboot.system.codegen.controller;

import com.code2roc.fastboot.system.codegen.model.SystemColumnDO;
import com.code2roc.fastboot.system.codegen.service.ISystemColumnService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/codegen/column")
public class SystemColumnController extends BaseBootController {
    @Autowired
    private ISystemColumnService service;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String table_id = ConvertOp.convert2String(params.get("table_id"));
        if (!StringUtil.isEmpty(table_id)) {
            sql += " and table_id = #{table_id}";
            paramMap.put("table_id", table_id);
        }
        List<SystemColumnDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

}
