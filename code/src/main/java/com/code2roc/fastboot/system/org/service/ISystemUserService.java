package com.code2roc.fastboot.system.org.service;

import com.code2roc.fastboot.system.org.model.SystemUserDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemUserService extends BaseBootService<SystemUserDO> {
    boolean checkExistUserByeDeptID(String dept_id);
}
