package com.code2roc.fastboot.system.setting.service.impl;

import com.code2roc.fastboot.system.setting.model.SystemCodeItemDO;
import com.code2roc.fastboot.system.setting.service.ISystemCodeItemService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SystemCodeItemServiceImpl extends BaseBootServiceImpl<SystemCodeItemDO> implements ISystemCodeItemService {
}
