package com.code2roc.fastboot.system.org.controller;

import com.code2roc.fastboot.system.org.bizlogic.RoleLogic;
import com.code2roc.fastboot.system.org.bizlogic.UserLogic;
import com.code2roc.fastboot.system.org.model.*;
import com.code2roc.fastboot.system.org.service.ISystemDeptService;
import com.code2roc.fastboot.system.org.service.ISystemRoleService;
import com.code2roc.fastboot.system.org.service.ISystemRoleSettingService;
import com.code2roc.fastboot.system.org.service.ISystemUserService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/org/rolesetting")
public class SystemRoleSettingController extends BaseBootController {
    @Autowired
    private ISystemRoleService roleService;
    @Autowired
    private ISystemDeptService deptService;
    @Autowired
    private ISystemUserService userService;
    @Autowired
    private ISystemRoleSettingService service;
    @Autowired
    private UserLogic userLogic;
    @Autowired
    private RoleLogic roleLogic;

    @ResponseBody
    @PostMapping("/getUserRelationList")
    public Object getUserRelationList(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String login_name = ConvertOp.convert2String(params.get("login_name"));
        String user_name = ConvertOp.convert2String(params.get("user_name"));
        String dept_id = ConvertOp.convert2String(params.get("dept_id"));
        if (dept_id.equals("Top")) {
            if (!StringUtil.isEmpty(login_name)) {
                sql += " and login_name like #{login_name}";
                paramMap.put("login_name", "%" + login_name + "%");
            }
            if (!StringUtil.isEmpty(user_name)) {
                sql += " and user_name like #{user_name}";
                paramMap.put("user_name", "%" + user_name + "%");
            }
        } else {
            String guidString = "";
            List<String> guidList = deptService.getAllSubDeptIDList(dept_id).stream().distinct().collect(Collectors.toList());
            if (guidList.size() == 0) {
                guidString = "''";
            } else {
                for (int i = 0; i < guidList.size(); i++) {
                    if (i == guidList.size() - 1) {
                        guidString += "'" + guidList.get(i) + "'";
                    } else {
                        guidString += "'" + guidList.get(i) + "',";
                    }
                }
            }
            sql += " and dept_id in (" + guidString + ")";
            if (!StringUtil.isEmpty(login_name)) {
                sql += " and login_name like #{login_name}";
                paramMap.put("login_name", "%" + login_name + "%");
            }
            if (!StringUtil.isEmpty(user_name)) {
                sql += " and user_name like #{user_name}";
                paramMap.put("user_name", "%" + user_name + "%");
            }
        }

        List<SystemDeptDO> deptDOList = deptService.selectAllList();
        List<SystemRoleDO> roleDOList = roleService.selectAllList();
        List<SystemRoleSettingDO> roleSettingDOList = service.selectAllList();
        List<SystemUserDO> rows = userService.selectPageList("*", sql, "sort_num desc", paramMap);
        for (SystemUserDO  entity:rows) {
            entity.setDeptFullPath(deptService.getFullPath(entity.getDept_id(),deptDOList));
            entity.setRoleList(userLogic.getUserRoleList(entity.getRow_id(),roleDOList,roleSettingDOList));
        }
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/updateRelation")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object updateRelation(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String user_id = ConvertOp.convert2String(params.get("user_id"));
        String role_id = ConvertOp.convert2String(params.get("role_id"));
        boolean regist = ConvertOp.convert2Boolean(params.get("regist"));
        roleLogic.updateRelation(user_id, role_id, regist);
        return result;
    }

    @ResponseBody
    @PostMapping("/updateUserRelation")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object updateRelation(@RequestBody UserRoleModel userRoleModel) {
        Result result = Result.okResult();
        roleLogic.updateUserRelation(userRoleModel);
        return result;
    }
}
