package com.code2roc.fastboot.system.enums;

import com.code2roc.fastboot.framework.datadic.IConvertEnumToCodeItem;

public class MenuEnum {
    public enum MenuOpenModel implements IConvertEnumToCodeItem {
        Iframe(10, "系统内"), NewPage(20, "新窗口");
        private int _value;
        private String _name;

        private MenuOpenModel(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "菜单打开方式";
        }
    }

    public enum SettingModel{
        RoleSetting(10, "角色授权"), DeptSetting(20, "部门授权"), UserSetting(30, "人员授权");
        private int _value;
        private String _name;

        private SettingModel(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }
    }

    public enum PublicStatus{
        Public(10, "公开"), NoPublic(20, "不公开");
        private int _value;
        private String _name;

        private PublicStatus(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }
    }
}
