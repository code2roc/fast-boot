package com.code2roc.fastboot.system.setting.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.system.org.model.SystemRoleDO;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;
import java.util.List;

@TableName("system_menu")
public class SystemMenuDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;
    @TableField(value = "sort_num")
    private Integer sort_num;
    @TableField(value = "gmt_create")
    private Date gmt_create;
    @TableField(value = "gmt_modified")
    private Date gmt_modified;
    @TableField("menu_name")
    private String menu_name;
    @TableField("menu_code")
    private String menu_code;
    @TableField("url")
    private String url;
    @TableField("icon")
    private String icon;
    @TableField("public_status")
    private Integer public_status;
    @TableField("open_mode")
    private Integer open_mode;
    @TableField("menu_status")
    private Integer menu_status;

    @TableField(exist = false)
    private String parent_menu_code;
    @TableField(exist = false)
    private List<SystemRoleDO> roleList;

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_code() {
        return menu_code;
    }

    public void setMenu_code(String menu_code) {
        this.menu_code = menu_code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getPublic_status() {
        return public_status;
    }

    public void setPublic_status(Integer public_status) {
        this.public_status = public_status;
    }

    public Integer getOpen_mode() {
        return open_mode;
    }

    public void setOpen_mode(Integer open_mode) {
        this.open_mode = open_mode;
    }

    public Integer getMenu_status() {
        return menu_status;
    }

    public void setMenu_status(Integer menu_status) {
        this.menu_status = menu_status;
    }

    @Override
    public String getRow_id() {
        return row_id;
    }

    @Override
    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    @Override
    public Integer getSort_num() {
        return sort_num;
    }

    @Override
    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    @Override
    public Date getGmt_create() {
        return gmt_create;
    }

    @Override
    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    @Override
    public Date getGmt_modified() {
        return gmt_modified;
    }

    @Override
    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }

    public String getParent_menu_code() {
        return parent_menu_code;
    }

    public void setParent_menu_code(String parent_menu_code) {
        this.parent_menu_code = parent_menu_code;
    }

    public List<SystemRoleDO> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SystemRoleDO> roleList) {
        this.roleList = roleList;
    }
}
