package com.code2roc.fastboot.system.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code2roc.fastboot.system.common.model.SystemJobDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemJobDao extends BaseMapper<SystemJobDO> {
}
