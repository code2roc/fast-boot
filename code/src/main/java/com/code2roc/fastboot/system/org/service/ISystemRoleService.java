package com.code2roc.fastboot.system.org.service;

import com.code2roc.fastboot.system.org.model.SystemRoleDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemRoleService extends BaseBootService<SystemRoleDO> {
}
