package com.code2roc.fastboot.system.codegen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code2roc.fastboot.system.codegen.model.SystemColumnDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemColumnDao extends BaseMapper<SystemColumnDO> {
}
