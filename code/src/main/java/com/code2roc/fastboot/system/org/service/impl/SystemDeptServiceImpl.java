package com.code2roc.fastboot.system.org.service.impl;

import com.code2roc.fastboot.system.org.model.SystemDeptDO;
import com.code2roc.fastboot.system.org.service.ISystemDeptService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SystemDeptServiceImpl extends BaseBootServiceImpl<SystemDeptDO> implements ISystemDeptService {
    @Override
    public List<String> getAllSubDeptIDList(String group_id) {
        List<String> guidlist = new ArrayList<String>();
        guidlist.add(group_id);
        List<SystemDeptDO> lst_All = selectAllList();
        selectSubList(lst_All, group_id, guidlist);
        return guidlist.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public boolean checkExistSub(String parent_id) {
        String sql = "parent_id = #{parent_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("parent_id", parent_id);
        return selectCount(sql, paramMap) > 0;
    }

    @Override
    public String getFullPath(String dept_id, List<SystemDeptDO> lst_All) {
        List<SystemDeptDO> path = new ArrayList<SystemDeptDO>();
        List<SystemDeptDO> list = lst_All.stream().filter((SystemDeptDO f) -> f.getRow_id().equals(dept_id)).collect(Collectors.toList());
        if (list.size() > 0) {
            SystemDeptDO dept = list.get(0);
            path.add(dept);
            if (!dept.getParent_id().contains("Top")) {
                selectSubPath(lst_All, dept.getParent_id(), path);
            }
        }
        //倒叙
        Collections.reverse(path);
        String fullpath = "";
        for (SystemDeptDO f : path) {
            fullpath += f.getDept_name() + " - ";
        }
        if (fullpath.endsWith(" - ")) {
            fullpath = fullpath.substring(0, fullpath.length() - 3);
        }
        return fullpath;
    }

    private void selectSubList(List<SystemDeptDO> lst_All, String parent_id, List<String> guidList) {
        List<SystemDeptDO> list = lst_All.stream().filter((SystemDeptDO f) -> f.getParent_id().equals(parent_id)).collect(Collectors.toList());
        if (list.size() > 0) {
            for (SystemDeptDO entity : list) {
                guidList.add(entity.getRow_id());
                selectSubList(lst_All, entity.getRow_id(), guidList);
            }
        }
    }

    private void selectSubPath(List<SystemDeptDO> lst_All, String dept_id, List<SystemDeptDO> path) {
        List<SystemDeptDO> list = lst_All.stream().filter((SystemDeptDO f) -> f.getRow_id().equals(dept_id)).collect(Collectors.toList());
        if (list.size() > 0) {
            SystemDeptDO dept = list.get(0);
            path.add(dept);
            if (!dept.getParent_id().contains("Top")) {
                selectSubPath(lst_All, dept.getParent_id(), path);
            }
        }
    }
}
