package com.code2roc.fastboot.system.setting.controller;

import com.code2roc.fastboot.system.setting.model.SystemConfigDO;
import com.code2roc.fastboot.system.setting.service.ISystemConfigService;
import com.code2roc.fastboot.system.setting.service.ISystemGroupService;
import com.code2roc.fastboot.system.enums.CommonEnum;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/setting/config")
public class SystemConfigController extends BaseBootController {
    @Autowired
    private ISystemConfigService service;
    @Autowired
    private ISystemGroupService groupService;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String config_name = ConvertOp.convert2String(params.get("config_name"));
        String group_id = ConvertOp.convert2String(params.get("group_id"));
        if(StringUtil.isEmpty(group_id)){
            if (!StringUtil.isEmpty(config_name)) {
                sql += " and config_name like #{config_name}";
                paramMap.put("config_name", "%" + config_name + "%");
            }
        }else{
            sql += groupService.getGroupIDSearchSQL(group_id);
            if (!StringUtil.isEmpty(config_name)) {
                sql += " and config_name like #{config_name}";
                paramMap.put("config_name", "%" + config_name + "%");
            }
        }

        List<SystemConfigDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/insert")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object insert(@RequestBody SystemConfigDO entity) {
        Result result = Result.okResult();
        if(service.checkExist("config_name",entity.getConfig_name())){
            return Result.errorResult().setMsg("系统参数已存在");
        }
        service.insert(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/detail")
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        result.add("obj", service.selectOne(row_id));
        return result;
    }

    @ResponseBody
    @PostMapping("/update")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object update(@UpdateRequestBody SystemConfigDO entity) {
        Result result = Result.okResult();
        if(service.checkExist("config_name",entity.getConfig_name(),entity.getRow_id())){
            return Result.errorResult().setMsg("系统参数已存在");
        }
        service.update(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/delete")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object delete(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        SystemConfigDO entity = service.selectOne(row_id);
        if(entity.getSystem_init()== CommonEnum.YesOrNo.Yes.get_value()){
            return Result.errorResult().setMsg("系统内置数据，禁止删除");
        }
        service.delete(row_id);
        return result;
    }

}
