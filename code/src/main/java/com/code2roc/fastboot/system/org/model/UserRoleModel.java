package com.code2roc.fastboot.system.org.model;

import java.util.List;

public class UserRoleModel {
    private String user_id;
    private List<RoleModel> roleModelList;

    public class RoleModel{
        private String role_id;
        private boolean regist;

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public boolean isRegist() {
            return regist;
        }

        public void setRegist(boolean regist) {
            this.regist = regist;
        }
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public List<RoleModel> getRoleModelList() {
        return roleModelList;
    }

    public void setRoleModelList(List<RoleModel> roleModelList) {
        this.roleModelList = roleModelList;
    }
}
