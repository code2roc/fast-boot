package com.code2roc.fastboot.system.common.service;

import com.code2roc.fastboot.system.common.model.SystemJobDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemJobService extends BaseBootService<SystemJobDO> {
}
