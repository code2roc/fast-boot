package com.code2roc.fastboot.system.setting.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code2roc.fastboot.system.setting.model.SystemGroupDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemGroupDao extends BaseMapper<SystemGroupDO> {
}
