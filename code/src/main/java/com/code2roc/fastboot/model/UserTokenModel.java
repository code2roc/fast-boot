package com.code2roc.fastboot.model;

import com.code2roc.fastboot.framework.auth.TokenModel;

public class UserTokenModel extends TokenModel {
    private String deptID;
    private String deptName;
    private String roleID;
    private String roleName;

    public UserTokenModel(){
        deptID = "";
        deptName = "";
        roleID = "";
        roleName = "";
    }

    public String getDeptID() {
        return deptID;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
