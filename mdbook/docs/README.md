# 快速上手

## 功能简介

fastboot内置一些了基础开发模块，说明如下

<table>
    <tr>
 		<td style="width:150px;text-align:center">模块名称</td>
        <td style="width:150px;text-align:center">功能名称</td>
        <td style="width:300px;text-align:center">简介说明</td>
    </tr>
    <tr>
        <td style="text-align:center" rowspan="4">组织架构</td>
         <td style="text-align:center">部门管理</td>
        <td>
            增/删/改/查部门
        </td>
    </tr>
    <tr>
        <td style="text-align:center">人员管理</td>
        <td>
             增/删/改/查人员
        </td>
    </tr>
    <tr>
        <td style="text-align:center">角色管理</td>
        <td>
             增/删/改/查角色
        </td>
    </tr>
    <tr>
        <td style="text-align:center">角色关系</td>
        <td>
            为人员配置相应的角色
        </td>
    </tr>
   <tr>
        <td style="text-align:center"  rowspan="5">系统配置</td>
        <td  style="text-align:center">菜单配置</td>
        <td>
             增/删/改/查菜单
       </td>
    </tr>
 <tr>
        <td style="text-align:center">菜单权限</td>
        <td>
             将菜单开放给不同角色/部门/人员
        </td>
    </tr>
    <tr>
        <td style="text-align:center">目录管理</td>
        <td>
             增/删/改/查目录
        </td>
    </tr>
    <tr>
        <td style="text-align:center">系统参数</td>
        <td>
            配置自定义参数
        </td>
    </tr>
    <tr>
        <td style="text-align:center">数据字典</td>
        <td>
            配置业务相关代码项，例如性别/是否/开启状态等
        </td>
    </tr>
      <tr>
        <td style="text-align:center" rowspan="4">系统监控</td>
        <td style="text-align:center">定时任务</td>
        <td>
            可视化配置/执行定时任务
         </td>
    </tr>
    <tr>
        <td style="text-align:center">附件管理</td>
        <td>
            上传/下载/查询/删除附件
        </td>
    </tr>
  	<tr>
        <td style="text-align:center">枚举字典</td>
        <td>
            查看通过枚举定义转换的代码项
        </td>
    </tr>
 	<tr>
        <td style="text-align:center">系统缓存</td>
        <td>
            查看系统所有缓存及详情，提供缓存刷新功能
        </td>
    </tr>
      <tr>
        <td style="text-align:center" rowspan="4">开发者</td>
        <td style="text-align:center">代码生成</td>
        <td>
            生成后端模板代码
         </td>
    </tr>
    	<tr>
        <td style="text-align:center">表单示例</td>
        <td>
            提供内置前端组件操作示例
        </td>
    </tr>
    	<tr>
        <td style="text-align:center">模块示例</td>
        <td>
            提供单表增/删/改/查页面模板示例
        </td>
    </tr>
    	<tr>
        <td style="text-align:center">源码查看</td>
        <td>
            跳转gitee代码管理参控股
        </td>
    </tr>
</table>

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/MainRemark2.png)

## 模块建表

系统建表需要包含几个固定字段

建表脚本如下

```sql
CREATE TABLE `表名` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

```

## 代码生成

进入菜单后台管理-开发者-代码生成-导入表结构，选择需要生成代码的表，表描述与字段描述在数据库对应注释填写，可自动同步过来

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/GenCode1.png)

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/GenCode2.png)

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/GenCode3.png)

点击编辑按钮填写生成代码相关设置，包括包名和路由，填写完毕后勾选表点击生成代码按钮，获取后端代码压缩包，包含DO，Dao，Serivce，ServiceImpl，Controller五个文件代码，前端页面代码不生成，需要开发参照表单示例及模块示例

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/GenCode4.png)

## 页面配置

进入菜单后台管理-系统配置-菜单管理，新增对应节点下的菜单，填写菜单名称/地址/图标等信息

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/MenuConfig1.png)

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/MenuConfig2.png)

进入菜单后台管理-系统配置-菜单权限，勾选对应开发角色或者点击设置按钮选择部门/人员

![](https://code2roc-fastboot.oss-cn-hangzhou.aliyuncs.com/MenuConfig3.png)

# 封装框架

fastboot提供全部源代码，可在其基础上进行拓展修改，也可以将标准版fastboot项目打包成功基础框架使用，

发布到到本地maven打包步骤如下（也可以发布到maven中央仓库或自建私服）

- 进行Build Artifacts，选择打包时拷贝链接
- 点击Maven插件LifeCycle中的Install按钮
- 将Build Artifacts后的fastboot.jar拷贝到本地maven仓库替换install后的jar包

创建新的springboot项目，修改一些配置既可以使用标准版fastboot框架

示例项目请查看example文件夹，启动访问地址：http://localhost:9002/fastmanage/index.html

- springboot版本修改为2.0.0.RELEASE

- dependecies修改为如下

```xml
    <dependencies>
        <dependency>
            <groupId>com.code2roc</groupId>
            <artifactId>fastboot</artifactId>
            <version>1.0.4</version>
        </dependency>

        <!-- mysql连接 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.21</version>
        </dependency>
    </dependencies>
```

- 启动增加扫描配置

```java
@MapperScan(basePackages = {"com.code2roc.fastboot.*.dao", "com.code2roc.fastboot.*.*.dao"})
@SpringBootApplication(scanBasePackages = {"com.code2roc"})
@ComponentScan(basePackages = {"com.code2roc"})
public class ManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManageApplication.class, args);
    }

}
```

- 创建webconfig继承BootWebConfig

```java
@Configuration
@EnableTransactionManagement
@EnableAspectJAutoProxy
@EnableAsync
public class ManageWebConfig extends BootWebConfig {
}

```

- 拷贝application.yml，application-dev.yml，logback-spring.xml三个配置文件到resources目录下

- 修改system.projectName和system.systemName

- resources文件夹下创建static/js文件夹，拷贝config.js，修改prefix和system.projectName保持一致

# 返回状态码

返回数据基本结构，详情见Result

```json
{
	"msg": "操作成功",
	"code": 0,
	"data": {
		
	}
}
```

详情见SystemErrorCodeEnum

| 状态码 | 错误信息                |
| ------ | ----------------------- |
| 0      | 默认请求成功            |
| -1     | 默认请求失败            |
| -1999  | 系统启动中              |
| -998   | 系统执行发生错误        |
| -997   | 系统运行发生错误        |
| -996   | 请求路径未找到          |
| -981   | 请求参数格式错误        |
| -980   | 重复请求，请稍后再试    |
| -975   | 提交信息存在SQL注入风险 |
| -850   | 无权限访问              |
| -840   | 数据源配置不正确        |

# 打包操作

默认打jar包不再详细说明

打war包增加以下两个额外处理

pom文件增加配置项

```xml
<packaging>war</packaging>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-tomcat</artifactId>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>3.1.0</version>
    <scope>provided</scope>
</dependency>
```

修改配置项

```json
system:
  package: war
```

# 应用接口文档

## 鉴权

### 登入

<table>
    <tr>
        <td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/auth/userLogin</td>
    </tr>
    <tr>
        <td rowspan="2">参数</td>
        <td>loginName</td>
        <td>登录名</td>
    </tr>
    <tr>
        <td>password</td>
        <td>密码</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"msg": "操作成功",
	"code": 0,
	"data": {
		"token": "xxxx",
		"tokenModel":{
			"userID":"xxxx",
			"userName":"xxxx",
			"loginName":"xxxx",
			"deptID":"xxxx",
			"deptName":"xxx",
			"roleID":"xxxx",
			"roleName":"xxxx"
		}
	}
}
            </pre>
        </td>
    </tr>  
 	<tr>
        <td>备注</td>
        <td colspan="2">
            userID：用户ID</br>
    		userName：用户名</br>
            deptID：部门ID</br>
    		deptName：部门名称</br>
            roleID：角色ID</br>
    		roleName：角色名</br>
        </td>
    </tr>
</table>

### 登出

<table>
    <tr>
        <td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/auth/userLogOut</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>token</td>
        <td>登录返回的凭据</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"msg": "操作成功",
	"code": 0,
	"data": {
	}
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2">
        </td>
	</tr>
</table>

### 获取token

<table>
    <tr>
        <td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/auth/getUserTokenInfo</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>token</td>
        <td>登录返回的凭据</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"msg": "操作成功",
	"code": 0,
	"data": {
		"obj":{
			"userID":"xxxx",
			"userName":"xxxx",
			"loginName":"xxxx",
			"deptID":"xxxx",
			"deptName":"xxx",
			"roleID":"xxxx",
			"roleName":"xxxx"
		}
	}
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2">
        </td>
	</tr>
</table>

### 校验token

<table>
    <tr>
        <td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/auth/checkUserInfo</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>token</td>
        <td>登录返回的凭据</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"msg": "操作成功",
	"code": 0,
	"data": {
	}
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2">
        </td>
	</tr>
</table>

## 菜单

### 获取顶级菜单

<table>
    <tr>
        <td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/setting/menu/getTopMenuList</td>
    </tr>
    <tr>
        <td>参数</td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"msg": "操作成功",
	"code": 0,
	"data": {
		"obj":[
			{
                "icon": "",
                "menu_code": "009900010001",
                "menu_name": "部门管理",
                "open_mode": 10,
                "row_id": "202209081036396d4f2b47-3a17-4b80-9acf-e216f974f13b",
                "url": "system/dept/dept_list.html"
			}
		]
	}
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2">
            row_id：菜单标识</br>
     		menu_name：菜单名称</br>
    		menu_code：菜单编码</br>
			url：菜单地址</br>
			icon：图标</br>
			open_mode：打开方式 10：系统内跳转 20：外链
        </td>
	</tr>
</table>

### 获取菜单节点下所有菜单

<table>
    <tr>
        <td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/setting/menu/getSubMenuTreeData</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>menu_id</td>
        <td>菜单唯一标识</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"code": 0,
	"data": {
		"obj": [{
				"id": "00990001",
				"name": "组织架构",
				"attributes": {
					"openmodel": 10,
					"icon": "layui-icon-set",
					"url": ""
				},
				"children": [{
					"id": "009900010001",
					"name": "部门管理",
					"attributes": {
						"openmodel": 10,
						"icon": "",
						"url": ""
					}
				}]
			}
		]
	},
	"msg": "success"
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2">
            id：菜单编码 </br>
            name：菜单名称 </br>
            children：子级菜单 </br>
            openmodel：打开方式 </br>
            icon：图标 </br>
            url：菜单地址 </br>
        </td>
	</tr>
</table>

## 附件

### 上传附件

<table>
    <tr>
 		<td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/attach/upload</td>
    </tr>
    <tr>
        <td rowspan="4">参数</td>
        <td>token</td>
        <td>用户凭据</td>
    </tr>
    <tr>
        <td>group_guid</td>
        <td>一般传入数据表主键</td>
    </tr>
    <tr>
        <td>group_type</td>
        <td>传入附件具体分类</td>
    </tr>
    <tr>
        <td>file</td>
        <td>文件</td>
    </tr>    
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"code": 0,
	"data": {
	},
	"msg": "success"
}
            </pre>
        </td>
    </tr>
    <tr>
        <td>备注</td>
        <td colspan="2">
            此接口传递参数格式为form-data</br>
        </td>
    </tr>      
</table>

### 附件列表

<table>
    <tr>
 		<td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/attach/list</td>
    </tr>
    <tr>
        <td rowspan="2">参数</td>
        <td>group_guid</td>
        <td>一般传入数据表主键</td>
    </tr>
    <tr>
        <td>group_type</td>
        <td>传入附件具体分类</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"code": 0,
	"data": {
		"total": 1,
		"rows": [{
			"attach_name": "《手撸 Spring》 • 小傅哥.pdf",
			"attach_size": 8599754,
			"attach_status": 10,
			"attach_type": "pdf",
			"attach_virtual_path": "default/202302061343596195d6cb-6c62-438c-b39a-8df5b1bac212/《手撸 Spring》 • 小傅哥.pdf",
			"gmt_create": "2023-02-06 13:44:00",
			"gmt_modified": "2023-02-06 13:44:00",
			"group_guid": "xxx",
			"group_type": "default",
			"row_id": "202302061343596195d6cb-6c62-438c-b39a-8df5b1bac212",
			"upload_user_id": "20220822112728225efcd4-3912-4f2d-b40b-87565505456c",
			"upload_user_name": "系统管理员"
		}]
	},
	"msg": "success"
}
            </pre>
        </td>
    </tr>
    <tr>
        <td>备注</td>
        <td colspan="2">
        </td>
	</tr> 
</table>     

### 删除附件

<table>
    <tr>
 		<td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/attach/delete</td>
    </tr>
    <tr>
        <td >参数</td>
        <td>row_id</td>
        <td>附件主键</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"code": 0,
	"data": {
	},
	"msg": "success"
}
            </pre>
        </td>
    </tr>
    <tr>
        <td>备注</td>
        <td colspan="2">
        </td>
	</tr> 
</table>  

### 下载附件

<table>
    <tr>
 		<td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/attach/download?row_id=xxxx</td>
    </tr>
     <tr>
        <td>参数</td>
        <td colspan="2"></td>
    </tr>    
     <tr>
        <td>响应</td>
        <td colspan="2"></td>
    </tr>    
    <tr>
        <td>备注</td>
        <td>
           直接get方式访问，row_id为附件主键
        </td>
    </tr>  
</table>

### 分片上传

<table>
    <tr>
 		<td style="width:150px">接口地址</td>
        <td style="width:800px" colspan="2">/system/common/attach/uploadChunkFile</td>
    </tr>
    <tr>
        <td rowspan="8">参数</td>
        <td>file</td>
        <td>文件</td>
    </tr>
    <tr>
        <td>fileMD5</td>
        <td>文件md5</td>
    </tr>
    <tr>
        <td>chunkCount</td>
        <td>分片数量</td>
    </tr>
    <tr>
        <td>chunkIndex</td>
        <td>分片编号（从0开始）</td>
    </tr>    
    <tr>
        <td>chunkSize</td>
        <td>分片大小</td>
    </tr>   
    <tr>
        <td>group_guid</td>
        <td></td>
    </tr>
     <tr>
        <td>group_type</td>
        <td></td>
    </tr>
      <tr>
        <td>attachGuid</td>
        <td>附件主键（传空或不传后端自动生成）</td>
    </tr>
    <tr>
    <td>响应</td>
    <td colspan="2">
        <pre>
{
	"code": 0,
	"data": {
	},
	"msg": "success"
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2">
            此接口传递参数格式为form-data</br>
			分片按顺序上传，一定要是同步请求</br>
        </td>
    </tr>    
</table>

## 代码项

### 枚举代码项

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:800px" colspan="2">/frame/enumcode/getCodeDetail</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>codeName</td>
        <td>代码项名称</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
    "code": 0,
    "data": {
        "obj": [
            {
                "itemText": "Male",
                "itemValue": 10
            },
            {
                "itemText": "f_usersextend",
                "itemValue": 20
            }
        ]
    },
    "msg": "success"
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2"></td>
    </tr>    
</table>

### 数据字典代码项

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:800px" colspan="2">/system/setting/codeitem/getCodeDetail</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>codeName</td>
        <td>代码项名称</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
    "code": 0,
    "data": {
        "obj": [
            {
                "itemText": "Male",
                "itemValue": 10
            },
            {
                "itemText": "f_usersextend",
                "itemValue": 20
            }
        ]
    },
    "msg": "success"
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2"></td>
    </tr>    
</table>

# 多数据源

## 配置

方式一：在yml文件中配置datasource下的cluster节点

```yaml
  # 从数据源（可配置多个cluster节点）
  cluster:
    - key: db2
      type: com.alibaba.druid.pool.DruidDataSource
      driverClassName: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306/kerneldemo-2?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai
      username: root
      password: 11111
```

方式二：自定义实现IExtraDataSourceRegist接口，系统会在启动时注册返回的DataSourceModel对象

```java
@Service
public class ExtraDataSourceConfig  implements IExtraDataSourceRegist {

    @Override
    public List<DataSourceModel> getExtraDataSourc() {
        List<DataSourceModel> dataSourceModelList = new ArrayList<>();
        return dataSourceModelList;
    }
}

```

## 数据切换

在service实现类中增加@TargetDataSource注解，值就是配置的从数据库的key，示例如下

```java
@Service
@TargetDataSource("db2")
public class NoticeServiceImpl extends BaseServiceImpl<NoticeDO> implements NoticeService {

}
```

## 单库事务

controller层增加@Transitional默认主库事务，示例如下

```java
    @ResponseBody
    @PostMapping("/insert")
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    @CacheTransactional
    public Object insert(@RequestBody UserDO entity) {
        Result result = Result.okResult();
        userService.insert(entity);
        return result;
    }
```

从库事务需要指定事务管理器名称，名称为从库配置key+TransactionManager,示例如下

```java
    @ResponseBody
    @PostMapping("/insert")
    @Transactional(transactionManager = "db2TransactionManager",rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object insert(@RequestBody NoticeDO entity) {
        Result result = Result.okResult();
        noticeService.insert(entity);
        return result;
    }
```

## 跨库事务

controller存在多个数据源组合操作使用事务需要指定注解@GlobalTransactional，示例如下

```java
    @ResponseBody
    @PostMapping("/testGlobalTransition")
    @GlobalTransactional
    public Object testGlobalTransitionFail(@RequestBody HashMap params) {
        Result result = Result.okResult();
        doTransitionFail();
        return result;
    }

    public void doTransitionFail(){
        DeptDO deptDO = new DeptDO();
        deptDO.setDeptGuid(CommonUtil.getNewGuid());
        deptDO.setDeptName("test-dept");
        deptDO.setDeptGuid("Top");
        deptService.insert(deptDO);
        OperationLogDO logDO = new OperationLogDO();
        logDO.setLogGuid(CommonUtil.getNewGuid());
        logDO.setLogContent("insert dept");
        logDO.setLogDate(new Date());
        logService.insert(logDO);
        throw new CustomerRuntimException("xxxx");
    }
```

# 缓存操作

注：单表缓存和个性化缓存都支持自定义配置缓存（对象数量，堆外缓存大小，磁盘缓存大小），不指定相关参数默认使用yml中的缓存设置

- CacheTable注解不指定参数为默认
- ICacheInitHandler接口getHeap，getOffheap，getDisk三个方法返回空为默认

## 单表缓存

数据库实体对象增加@CacheTable注解，serivce和serviceimpl分别继承BaseCacheService和BaseCacheServiceImpl，系统启动时会自定将表中数据加载到缓存中，默认Service操作会和数据库同步，自定义扩展方法需要通过CacheUtil操作缓存，示例如下

```java
@TableName("demo_user")
@HPCache(heap = "50",offheap = "50",disk = "80")
public class UserDO extends BaseModel {
    @TableId(value = "UserGuid")
    private String userGuid;
    @TableField(value = "LoginName")
    private String loginName;
    @TableField(value = "DisplayName")
    private String displayName;
    @TableField(value = "Password")
    private String password;
    @TableField(value = "Birthday")
    private Date birthday;
    @TableField(value = "Age")
    private Integer age;
    @TableField(value = "Sex")
    private Integer sex;
    @TableField(value = "DeptGuid")
    private String deptGuid;
}
```

```java
public interface UserService extends BaseCacheService<UserDO> {
    
}

```

```java
@Service
public class UserServiceImpl extends BaseCacheServiceImpl<UserDO> implements UserService{

}
```

## 自定义缓存

新建缓存初始化Service实现ICacheInitHandler接口，存储的缓存对象需要继承BaseSystemObejct，操作缓存使用CacheUtil工具类，示例如下

```java
public class SystemBaseInfo extends BaseSystemObject {
    private String systemName;
    private String systemAuthor;
    private String systemVersion;

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemAuthor() {
        return systemAuthor;
    }

    public void setSystemAuthor(String systemAuthor) {
        this.systemAuthor = systemAuthor;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }
}
```

```java
@Service
public class SystemInfoCacheService implements ICacheInitHandler {
    @Autowired
    private CacheUtil cacheUtil;

    @Override
    public String getCacheName() {
        return "SystemInfo";
    }

    @Override
    public void initCache() {
        SystemBaseInfo systemInfo = new SystemBaseInfo();
        systemInfo.setSystemName("kernel-demo");
        systemInfo.setSystemAuthor("code2roc");
        systemInfo.setSystemVersion("1.0.0");
        cacheUtil.putCache(getCacheName(), "baseInfo", systemInfo);
    }

    @Override
    public String getHeap() {
        return "10";
    }

    @Override
    public String getOffheap() {
        return "10";
    }

    @Override
    public String getDisk() {
        return "20";
    }    
}
```

## 初始化方式

缓存初始化默认异步，需要同步时增加@SyncInit注解，单表缓存标注DO实体类，自定义缓存标注缓存初始化实现类，示例如下

```java
@TableName("demo_user")
@CacheTable
@SyncInit
public class UserDO extends BaseModel {

}
```

## 缓存事务

当缓存对应的数据库变动需要支持事务，controller层需要增加@CacheTransitional注解保证缓存与数据库的一致性，代码如下

```java
    @ResponseBody
    @PostMapping("/testCacheTransition")
    @CacheTransactional
    public Object testCacheTransition(@RequestBody HashMap params) {
        Result result = Result.okResult();
        doCacheTransitionFail();
        return result;
    }

	public void doCacheTransitionFail(){
        DeptDO deptDO = new DeptDO();
        String deptGuid = "test-dept-2";
        deptDO.setDeptGuid(deptGuid);
        deptDO.setDeptName("test-dept-2");
        deptDO.setDeptGuid("Top");
        deptService.insert(deptDO);
        UserDO userDO = new UserDO();
        userDO.setDeptGuid(deptGuid);
        userDO.setLoginName("18066953300");
        userDO.setDisplayName("测试用户");
        userDO.setUserGuid(CommonUtil.getNewGuid());
        userService.insert(userDO);
        throw new CustomerRuntimException("doCacheTransitionFail");
    }
```

## 业务层面控制缓存

对于已存在的稳定业务代码想要加入缓存，可能因为涉及的操作代码较多较分散，或存在直接执行sql语句更新数据库的情况，无法从db操作层面对service进行统一控制

如果涉及到的缓存数据对应表的业务层面（接口路由对应的方法相对集中），则可以通过对变更数据的代码添加@AutoRefreshCache注解来进行统一缓存同步

```java
    @ResponseBody
    @RequestMapping("/update")
    @AutoRefreshCache("SystemMenu")
    public Object update(@RequestBody F_MenumanageDO f_Menumanage) {
        f_menuLogic.update(f_Menumanage);
        return R.ok();
    }
```

# 身份鉴权

## token携带

生成的token前端传递时可以通过header头信息或者url参数携带，参数名称均为token

```javascript
function getheadinfo() {
    var headinfo =  {
        "Content-Type": "application/json;charset=uft-8",
        "token": GetTokenID(),
    };
    return headinfo;
}
```

## token获取

后端通过AuthUtil获取用户身份信息对象

```java
  UserTokenModel userTokenModel = authUtil.getUserTokenModel();
```

## 匿名访问

方式一：yml文件中的security中的allowAnonymousAccess增加配置项

```yaml
security:
  # 匿名访问
  allowAnonymousAccess: ["/example/login/userLogin"]
```

方式二：controller方法或者类增加@AnonymousAccess注解

```java
    @ResponseBody
    @PostMapping("/getCodeDetail")
    @AnonymousAccess
    public Object getCodeDetail(@RequestBody Map<String, Object> params){
        String codeName = ConvertOp.convert2String(params.get("codeName"));
        if(StringUtil.isEmpty(codeName)){
            return Result.errorResult().add("msg","代码项名称不能为空");
        }
        List<CodeItem> codeItemList = DataDicManage.getCodeList(codeName);
        if(null==codeItemList){
            return Result.errorResult().add("msg","代码项不存在");
        }else{
            return Result.okResult().add("obj",codeItemList);
        }

    }
```

## 身份模拟

实现身份模拟接口IMockIdentify，有且只能有一个实现，模拟的代码方法上增加@MockIdentify注解并指定loginName参数，@MockIdentify注解已集成匿名访问，身份模拟优先级大于携带token，会进行覆盖，示例如下

```java
@Service
public class CustomerMockService implements IMockIdentify {
    @Autowired
    private UserService userService;
    @Autowired
    private DeptService deptService;
    @Autowired
    private TokenUtil tokenUtil;

    @Override
    public TokenModel getMockTokenModel(String loginName) {
        UserDO userDO = userService.selectOneByLoginName(loginName);
        //生成Token
        UserTokenModel userTokenModel = new UserTokenModel();
        userTokenModel.setUserID(userDO.getUserGuid());
        userTokenModel.setUserName(userDO.getDisplayName());
        userTokenModel.setLoginName(loginName);
        DeptDO deptDO = deptService.selectOne(userDO.getDeptGuid());
        if (deptDO != null) {
            userTokenModel.setDeptGuid(deptDO.getDeptGuid());
            userTokenModel.setDeptName(deptDO.getDeptName());
        }
        tokenUtil.createToken(userTokenModel);
        return userTokenModel;
    }
}
```

```java
    @ResponseBody
    @PostMapping("/getMockUserInfo")
    @MockIdentify(loginName = "zhangsan")
    public Object getMockUserInfo(@RequestBody HashMap params) {
        Result result = Result.okResult();
        return result;
    }
```

## 接口权限

自定义权限逻辑实现接口IPermissionAuth，自定义权限注解组合@PermissionAuth注解并指定实现类，代码如下

```java
@Service
public class UserPermissionAuth implements IPermissionAuth {
    @Autowired
    private TokenUtil tokenUtil;

    @Override
    public boolean checkDataAccess() {
        return tokenUtil.getTokenModel().getUserID().equals("admin");
    }
}
```

```java
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
@PermissionAuth(policy = UserPermissionAuth.class)
public @interface UserPermission {
    
}
```

```java
    @ResponseBody
    @PostMapping("/testAuth")
    @UserPermission
    public Object testAuth(@RequestBody HashMap params) {
        Result result = Result.okResult();
        return result;
    }
```

# 消息队列

## 通道配置

在yml文件中system节点下的messageChannel项配置，默认使用redis的发布模式，可切换成activemq，需要安装activemq服务，且在spring下增加相关配置

```yaml
system:
  # 消息模式 redis/activemq
  messageChannel: redis
```

```yaml
spring:
  activemq:
   broker-url: tcp://127.0.0.1:61616 #MQ服务器地址
   user: admin
   password: admin
   pool:
     enabled: true
```

注：安装activemq版本为5.15.0和jdk1.8版本对应

## 消息处理

不同种类的消息处理分别实现IMessageReceiver接口进行消息接收

```java
@Service
public class LogMessageReceiver implements IMessageReceiver {
    @Autowired
    private OperationLogService logService;

    @Override
    public void handleMessage(Object bodyObject, HashMap extraParam) {
        OperationLogDO logDO = (OperationLogDO)bodyObject;
        logService.insert(logDO);
        System.out.println("OperationLog 处理完毕");
    }
}
```

## 消息发送

使用MessageUtil进行相关消息的数据发送

```java
    @ResponseBody
    @PostMapping("/testSendMessage")
    public Object testSendMessage(@RequestBody HashMap params) {
        Result result = Result.okResult();
        OperationLogDO logDO = new OperationLogDO();
        logDO.setLogGuid(CommonUtil.getNewGuid());
        logDO.setLogDate(new Date());
        logDO.setLogContent("testSendMessage-" + DateUtil.getDateFormatToMillSecond());
        messageUtil.sendMessage(logDO, LogMessageReceiver.class, null);
        return result;
    }
```

# Socket消息

系统提供websocket/sse两种推送消息通道，必须个性化接入身份鉴权

## 客户端（websocket）

客户端通过访问内置的websocket接口来和服务端创建连接交互消息

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:800px">frame/socket/系统token的值</td>
    </tr>
    <tr>
        <td>参数</td>
        <td></td>
    </tr>    
    <tr>
        <td>响应</td>
        <td>
            <pre>
{
	"code": 0,
	"data": {
		"socketType": "ToDo",
		"socketHandle": "NewToDO"
	},
	"msg": "新的待办"
}
            </pre>
        </td>
    </tr>   
    <tr>
        <td>备注</td>
        <td colspan="2">
            socketType：消息类型</br>
			socketHandle：消息行为</br>
        </td>
    </tr>    
</table>

为了保持websocket的心跳，客户端可以定时向服务端发送心跳消息，内容为heartbeat，会自动返回应答

## 客户端（sse）

创建连接

<table>
    <tr>
 		<td style="width:150px">接口地址</td>
        <td style="width:800px">common/sse/createConnect</td>
    </tr>
    <tr>
        <td>参数</td>
        <td></td>
    </tr>    
    <tr>
        <td>响应</td>
        <td>
            <pre>
{
	"code": 0,
	"data": {
		"socketType": "ToDo",
		"socketHandle": "NewToDO"
	},
	"msg": "新的待办"
}
            </pre>
        </td>
    </tr>   
    <tr>
        <td>备注</td>
        <td colspan="2">
           url参数附加token和clientID（前端生成好的）
        </td>
    </tr>    
</table>

关闭连接

<table>
    <tr>
 		<td style="width:150px">接口地址</td>
        <td style="width:800px">common/sse/closeConnect</td>
    </tr>
    <tr>
        <td>参数</td>
        <td></td>
    </tr>    
    <tr>
        <td>响应</td>
        <td>
            <pre>
{
	"code": 0,
	"data": {
		"socketType": "ToDo",
		"socketHandle": "NewToDO"
	},
	"msg": "新的待办"
}
            </pre>
        </td>
    </tr>   
    <tr>
        <td>备注</td>
        <td colspan="2">
           url参数附加token和clientID（前端生成好的）
        </td>
    </tr>    
</table>

## 服务端

使用SocketUtil工具类来发送websocket消息，发送消息包含一下几个部分

- socketMessage——消息内容
- userID——用户登录后token中的userID标识
- socketType——消息类型
- socketHandle——消息动作
- extraData——额外数据（非必填，重载方法）

```java
    @ResponseBody
    @PostMapping("/testSendSocketMessage")
    public Object testSendSocketMessage(@RequestBody HashMap params) {
        Result result = Result.okResult();
        socketUtil.pushSocket("测试消息","xxxx","test","testmessage");
        return result;
    }
```

# 系统安全

系统提供一些常见的安全防护错误，在配置文件中的security节点进行修改

```yaml
security:
  # 跨域
  allowedOrigin: ["*"]
  # 匿名访问
  allowAnonymousAccess: []
  # xss防御
  openXSS: true
  # sql注入防御
  openSQLInject: true
  # 验证码过期时间，单位：秒
  validateCodeExpire: 60
  # token鉴权
  openTokenAuth: true
  # token过期时间（单位：分钟）
  tokenExpire: 180
  # 限制http方位方法为post get
  limitHTTPMethod: false
  # basic认证用户名
  basicUserName: admin
  # basic认证密码
  basicPassword: 11111
```

## 使用HTTPS

项目配置HTTPS访问，修改配置文件security节点下的相关配置

```yaml
security:
  # 开启https
  openSSL: false
  # ssl证书位置
  sslStore: classpath:systemfile/frame.jks
  # ssl证书密码
  sslStorePassword: abcd@1234
  # ssl证书类型
  sslStoreType: JKS
  # ssl证书名称
  sslalias: tomcat
  # 需要重定向到https的端口
  sslNeedRedirectPort: 8092
```

注意事项

- 此配置只针对jar包有效，war需要自行在部署容器配置
- 证书文件需要存储systemfile文件夹避免打包编码导致无法使用
- 监听重定向端口如果不使用，可去除sslNeedRedirectPort配置项或配置为0
- 开启limitHTTPMethod参数为true后ssl重定向功能无效

## 使用Basic认证

项目配置Basic，修改配置文件security节点下的相关配置

此用户名密码为默认，可通过注解参数指定，赋值格式【用户:密码】

```yaml
security:
  # basic认证用户名
  basicUserName: admin
  # basic认证密码
  basicPassword: 11111
```

使用示例如下

GET请求浏览器访问自动跳出输入界面，输入正确点击确定按钮自动返回对应接口结果

POST请求可以通过Header携带Authorization参数，值为【Basic Base64（用户:密码）】

GET/POST方式均支持Url直接携带Authorization参数进行认证

```java
    @ResponseBody
    @CheckBasic
    @GetMapping("/detailV2")
    public Object detailV2() {
        Result result = Result.okResult();
        result.add("basic信息","hello word v2");
        return result;
    }

    @ResponseBody
    @CheckBasic("sa:11111")
    @GetMapping("/detailV3")
    public Object detailV3() {
        Result result = Result.okResult();
        result.add("basic信息","hello word v3");
        return result;
    }
```

## 数据传输加密

提供接口请求参数传输加密功能，只支持application/json方式，后端无需额外改造，前端改步骤如下

- 随机生成8位数字字母字符串作为DES加密密钥**DESEncrtpt**
- 引入crypto-js.js对传入的json字符串数据进行DES加密得到密文参数**encryptData**
- 调用getRSAPublickKey接口获取RSA公钥
- 引入jsencrypt.js对**DESEncrtpt**密钥进行RSA加密得到加密后的密钥**Encrtpt**
- ajax请求时参数格式统一为{"encryptParam":**encryptData**}
- ajax请求时header增加encrypt参数，内容为**Encrtpt**

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:800px" colspan="2">/frame/excrypt/getRSAPublickKey</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>key</td>
        <td>随机生成键</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"code": 0,
	"data": {
		"obj": "xxxxx"
	},
	"msg": "success"
}
            </pre>
        </td>
    </tr>   
    <tr>
        <td>备注</td>
        <td colspan="2">返回加密公钥</td>
    </tr>
</table>

## 配置文件加密

提供配置文件配置项值加密操作，避免数据库/redis用户名密码等敏感数据泄露

加密方式为使用DESEncryptUtil进行默认加密，密钥为KernelXA

将加密后的配置项密文值进行固定标识组合重新配置yml文件，格式为KENC#+密文

```yaml
 username: KENC#zj1MpA60LZA=
```

## 返回值包装

提供对接口返回数据独立包装功能，例如返回的单条数据或列表数据时需要进行多表联查或者代码项值与文字的转换等功能，可通过缓存或计算等方式动态追加，避免DO类频繁扩展或创建多个VO类

实现扩展接口BaseWrapper，指定被扩展的对象，进行相关数据操作

```java
@Component
public class AccountWrapper implements BaseWrapper<AccountDO> {
    @Override
    public Map<String, Object> doWrap(AccountDO beWrappedModel) {
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("wrapperTag","test");
        return resultMap;
    }
}
```

接口添加Wrapper注解，指定扩展类

```java
    @ResponseBody
    @PostMapping("/detail")
    @Wrapper(AccountWrapper.class)
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String unitguid = ConvertOp.convert2String(params.get("unitguid"));
        result.add("obj", accountCacheService.selectOne(unitguid));
        return result;
    }
```

## 数据脱敏

脱敏操作在具体返回数据时触发，不影响接口内部数据二次操作

针对不同字段实现脱敏接口BaseSensitive

```java
@Component
public class AccountNametSensitive implements BaseSensitive {
    @Override
    public String doSensitive(String beSensitiveValue) {
        return beSensitiveValue.replace("test","****");
    }
}
```

使用SensitiveField注解标注返回数据的模型对象字段，并制定具体脱敏实现

```java
    @TableField(value = "AccountName")
    @SensitiveField(AccountNametSensitive.class)
    private String accountName;
```

使用Sensitive注解标注具体接口方法，触发脱敏操作

```java
    @ResponseBody
    @PostMapping("/test")
    @Sensitive
    public Object test(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        List<AccountDO> accountDOList =  accountService.selectList("*", sql, "id desc", paramMap);
        result.add("obj",accountDOList);
        return result;
    }
```

# 常用操作

## 静态文件

### 获取

系统内部代码需要获取的word模板等静态文件放入resources/systemfile文件夹下，maven需要配置相关路径防止打包后文件被编码损坏，且文件名一定是英文（jar包情况下，war包可存在中文），通过ClassPathFileUtil工具类获取

```xml
  <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
        <resources>
            <resource>
                <filtering>true</filtering>
                <directory>src/main/resources</directory>
                <excludes>
                    <exclude>systemfile/*</exclude>
                </excludes>
            </resource>
            <resource>
                <filtering>false</filtering>
                <directory>src/main/resources</directory>
                <includes>
                    <include>systemfile/*</include>
                </includes>
            </resource>
        </resources>
    </build>
```

```java
    @ResponseBody
    @PostMapping("/testGetStaticFileContent")
    @UserPermission
    public Object testGetStaticFileContent(@RequestBody HashMap params) {
        Result result = Result.okResult();
        String fielPath = ClassPathFileUtil.getFilePath("systemfile/HealthKnowledge.txt");
        String fileContent = FileUtil.getFileContent(fielPath);
        result.add("fileContent",fileContent);
        return result;
    }
```

### 下载

系统中需要直接访问下载的静态文件，配置yml文件下system节点的staticLocations选项，例如

```yaml
system:
  # 静态文件下载路径
  staticLocations: classpath:static/,classpath:systemfile/,file:templatefiles/download
```

classpath开头配置resource下的路径，file开头配置硬盘路径，访问地址如下所示

ip+端口号/项目名称/（针对配置的staticLocations的相对路径）

文件路径：resources/systemfile/HealthKnowledge.txt

访问路径：http://localhost:8091/fastboot/HealthKnowledge.txt

## 树形结数据处理

在开发过程中遇到树形的数据结构例如部门/人员等，返回给前端需要进行处理，这里提供了一套标准的方案

需要以下5个步骤自动生成树形结构数据，示例代码如下

- 查询树包含的所有数据
- 遍历数据列表
- 针对每条数据创建树节点对象TreeNode
- 设置节点名称/ID/父级ID/是否是顶级节点/添加额外信息
- 调用TreeBuilder.BuildTree方法

```java
    @ResponseBody
    @PostMapping("/tree")
    public Object getTreeData(HttpServletRequest request) {
        List<DeptDO> deptList = deptService.selectList("*","1=1","",new HashMap<>());
        //部门列表转化为树对象列表
        List<TreeNode> treeNodeList = new ArrayList<TreeNode>();
        for (DeptDO deptDO:deptList) {
            TreeNode tree = new TreeNode();
            tree.setId(deptDO.getDeptGuid());
            tree.setParentId(deptDO.getParentDeptGuid());
            tree.setLabel(deptDO.getDeptName());
            if(deptDO.getParentDeptGuid().contains("Top")){
                tree.setTopNode(true);
            }
            tree.addAttribute("deptCode",deptDO.getDeptCode());
            treeNodeList.add(tree);
        }
        List<TreeNode> result = TreeBuilder.BuildTree(treeNodeList);
        return Result.okResult().add("obj", result);
    }
```

## 异步任务

在Controller请求中执行异步任务时，异步方法标注需要指定共享request执行器【shareTaskExecutor】，解决了请求时执行异步任务Request无法共享问题，异步方法不能直接写在controller中，否则异步无效

```java
@Component
public class AsyncUtil {
    @Autowired
    private CommonDTO commonDTO;

    @Async("shareTaskExecutor")
    public void executeAsync () throws InterruptedException {
        System.out.println("开始执行executeAsync");
        Thread.sleep(3000);
        commonDTO.selectList("f_users","*","1=1","",new HashMap<>());
        System.out.println("结束执行executeAsync");
    }
}
```

## 重复提交

为了防止用户重复点击按钮提交数据造成数据重复异常，针对所有涉及到对数据新增接口的操作都要加上@SubmitLock注解，默认调用间隔3秒，可以通过expire属性设置间隔时间，示例如下

```java
	@ResponseBody
	@PostMapping("/insert")
	@SubmitLock(expire = 3)
	public Object insert(@RequestBody UserDo entity) {
		Result result =Result.okResult();
		userService.insert(entity);
		return result;
	}
```

# 扩展操作

## 文件操作

默认支持本地存储与minio存储两种方式，手动声明IFileOperate接口的Bean对象，示例如下

```java
    @Bean
    public IFileOperate fileOperate() {
        LocalFileStoargeProperty localFileStoargeProperty = new LocalFileStoargeProperty("attachfiles/");
        return new LocalFileOperate(localFileStoargeProperty);
    }

    @Bean
    public IFileOperate fileOperate() {
        MinioFileStoargeProperty minioFileStoargeProperty = new MinioFileStoargeProperty("http://192.168.1.164:9999/","admin","abcd@1234");
        return new MinioFileOperate(minioFileStoargeProperty);
    }
```

对接其他存储方式时（例如阿里云/华为云等），实现IFileOperate接口即可，一般不同的存储方式会创建一个配置类在构造函数初始化，完成后声明fileOperate时指定具体实现类即可

## 枚举数据字典

### 统一定义

基本数据结构如下，实现IConvertEnumToCodeItem接口指定代码项名称

```java
    public enum Sex implements IConvertEnumToCodeItem {
        Male(10, "Male"), FeMale(20, "f_usersextend");
        private int _value;
        private String _name;
        private Sex(int value, String name) {
            set_value(value);
            set_name((name));
        }
        public int get_value() {
            return _value;
        }
        public void set_value(int _value) {
            this._value = _value;
        }
        public String get_name() {
            return _name;
        }
        public void set_name(String _name) {
            this._name = _name;
        }
        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "性别";
        }
    }
```

### 统一获取

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:800px" colspan="2">/frame/enumcode/getCodeDetail</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>codeName</td>
        <td>代码项名称</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
    "code": 0,
    "data": {
        "obj": [
            {
                "itemText": "Male",
                "itemValue": 10
            },
            {
                "itemText": "f_usersextend",
                "itemValue": 20
            }
        ]
    },
    "msg": "success"
}
            </pre>
        </td>
    </tr>  
    <tr>
        <td>备注</td>
        <td colspan="2"></td>
    </tr>    
</table>

## 邮件发送

### 配置

配置文件中修改mail节点下的内容

```yaml
mail:
  # 邮箱
  userName: code2roc@163.com
  # 授权码
  password: xxxxx
  # 服务器地址
  host: smtp.163.com
  # 端口
  port: tempfiles/cache/
```

### 使用

调用MailUtil发送邮件，支持文本/富文本/附件等形式的邮件

```java
 @ResponseBody
    @PostMapping("/sendText")
    public Object sendText(@RequestBody Map<String, Object> params) {
        List<String> receiverList = new ArrayList<>();
        receiverList.add("280916680@@qq.com");
        MailUtil.sendSimpleMail("测试文本邮件", "sendText-测试文本邮件", receiverList);
        return Result.okResult();
    }

    @ResponseBody
    @PostMapping("/sendHtml")
    public Object sendHtml(@RequestBody Map<String, Object> params) throws Exception {
        List<String> receiverList = new ArrayList<>();
        receiverList.add("280916680@@qq.com");
        MailUtil.sendMimeMail("测试富文本邮件", "<b style='color:red'>sendHtml-测试富文本邮件</b>", receiverList);
        return Result.okResult();
    }

    @ResponseBody
    @PostMapping("/sendAttach")
    public Object sendAttach(@RequestBody Map<String, Object> params) throws Exception {
        List<String> receiverList = new ArrayList<>();
        receiverList.add("280916680@@qq.com");
        List<MailAttachModel> mailAttachModels = new ArrayList<>();
        MailAttachModel model = new MailAttachModel();
        model.setAttachName("测试附件-健康小知识");
        String filePath = ClassPathFileUtil.getFilePath("systemfile/HealthKnowledge.txt");
        model.setFilePath(filePath);
        mailAttachModels.add(model);
        MailUtil.sendMimeMail("测试附件邮件", "<b style='color:red'>sendAttach-测试附件邮件</b>", receiverList, mailAttachModels);
        return Result.okResult();
    }
```

## 验证码

提供了两个验证码接口，获取验证码，校验验证码，直接调用即可，与业务无耦合，如果想和业务结合调用ValidateCodeUtil验证码工具类的getBase64ValidateImage和checkValidate方法即可

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:800px" colspan="2">/frame/validatecode/getValidateCode</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>key</td>
        <td>随机生成键</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
	"code": 0,
	"data": {
		"obj": "data:image/gif;base64,xxxxx"
	},
	"msg": "success"
}
            </pre>
        </td>
    </tr>   
    <tr>
        <td>备注</td>
        <td colspan="2">返回base64图片字符</td>
    </tr>
</table>

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:800px" colspan="2">/frame/validatecode/checkValidateCode</td>
    </tr>
    <tr>
        <td>参数</td>
        <td>key</td>
        <td>getValidateCode传递的key</td>
    </tr>
     <tr>
        <td>参数</td>
        <td>validteCode</td>
        <td>输入的验证码</td>
    </tr>
    <tr>
        <td>响应</td>
        <td colspan="2">
            <pre>
{
    "code": 0,
    "data": {},
    "msg": "success"
}
            </pre>
        </td>
    </tr>   
    <tr>
        <td>备注</td>
        <td colspan="2"></td>
    </tr>
</table>

## SQL语句日志

执行操作数据库代码，系统会输出SQL语句打印到控制台和日志文件，在某些场景下，频繁的请求某些数据，例如请求日志/定时任务，我们需要将这些日志进行关闭，系统内置了@IgnoreSqlLog注解用于关闭某个类/某个执行方法产生的SQL语句输出

```java
    @ResponseBody
    @PostMapping("/list")
    @IgnoreSqlLog
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        return result;
    }
```

## 路由覆盖

在个性化接口类增加@CoverRoute注解，指定需要覆盖的路由地址，创建相同路由路径的的方法即可，访问原来的接口地址会自动转发到项目个性化接口地址，示例如下

原接口

```java
@Controller
@RequestMapping("/example/original")
public class RedirectOriginalExampleController {

    @PostMapping("/getConfig")
    @ResponseBody
    @AnonymousAccess
    public Object getConfig(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        result.add("tag","original");
        return result;
    }
}
```

覆盖后的接口

```java
@Controller
@RequestMapping("/example/redirect")
@CoverRoute("/example/original")
public class RedirectExampleController {

    @PostMapping("/getConfig")
    @ResponseBody
    @AnonymousAccess
    public Object getConfig(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String param1 = ConvertOp.convert2String(params.get("param1"));
        result.add("tag","redirect");
        result.add("param1",param1);
        return result;
    }
}
```

## 主动取消耗时任务

取消接口后台示例代码如下，controller增加@HandleCancel注解，如果有事物处理的需要主动try/catch进行回滚

```java
 	@PostMapping("/testTimeOut")
    @ResponseBody
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    @HandleCancel
    public Object testTimeOut(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        try {
            AccountDO entity1 = new AccountDO();
            entity1.setAccountName("before");
            accountService.insert(entity1);
            Thread.sleep(6000);
            AccountDO entity2 = new AccountDO();
            entity2.setAccountName("after");
            accountService.insert(entity2);
            System.out.println("testTimeOut-exe-finish");
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        return result;
    }
```

提供主动取消接口供前端调用

<table>
    <tr>
 		<td style="width:100px">接口地址</td>
        <td style="width:750px" colspan="2">/frame/handlecancel/killUserHandleThread</td>
    </tr>
    <tr>
        <td>响应</td>
        <td>
            <pre>
{
	"code": 0,
	"data": {},
	"msg": "success"
}
            </pre>
        </td>
    </tr> 
 	<tr>
        <td>备注</td>
        <td colspan="2"></td>
    </tr>    
</table>