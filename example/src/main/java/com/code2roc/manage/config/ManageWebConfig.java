package com.code2roc.manage.config;

import com.code2roc.fastboot.config.BootWebConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableAspectJAutoProxy
@EnableAsync
public class ManageWebConfig extends BootWebConfig {
}
